from setuptools import setup

setup(
    name='Triplets',
    version='21.01',
    packages=['Triplets',
              'Triplets.application',
              # 'Triplets.application.crynosphere',
              # 'Triplets.application.land',
              # 'Triplets.application.ocean',
              'Triplets.formation',
              'Triplets.models',
              'Triplets.processing',
              'Triplets.statistics'],
    author='Yan Yuan',
    author_email='Yan.Yuan@tudelft.nl',
    description='',
    install_requires=['numpy', 'scipy', 'osgeo', 'matplotlib', 'drama', 'xarray', 'imageio']
)