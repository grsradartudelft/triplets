import os
import glob
import gdal
import imageio
import xarray as xr
import numpy as np
import drama.utils as drtls
import scipy.stats as stats
import matplotlib.pyplot as plt

def cohs_to_closure_std(coh12, coh23, coh13, nlook, nrep=1024):
    """Short summary.

    Parameters
    ----------
    coh1 : type
        Description of parameter `coh1`.
    coh2 : type
        Description of parameter `coh2`.
    coh3 : type
        Description of parameter `coh3`.
    nlook : type
        Description of parameter `nlook`.

    Returns
    -------
    type
        Description of returned object.

    """
    cov = np.identity(3, dtype=complex)
    cov[0, 1] = coh12
    cov[0, 2] = coh13
    cov[1, 2] = coh23
    cov[1, 0] = coh12
    cov[2, 0] = coh13
    cov[2, 1] = coh23
    try:
        c = np.linalg.cholesky(cov)
        ndim = (nrep, int(nlook), 3)
        # White noise
        wn = (np.random.randn(*ndim) +  1j * np.random.randn(*ndim)) / np.sqrt(2)
        # Now we generate colored noise
        cn = np.einsum('ijk,mk->ijm', wn, c)
        triplet = np.roll(cn, -1, axis=2) * np.conj(cn)
        # Multilook
        cohs = np.mean(triplet, axis=1)
        # Calculate phase closure
        closure_phasor = np.prod(cohs, axis=1)
        closure_phase = np.angle(closure_phasor)
        # sigma_cp = np.std(closure_phase)
        stats_cp = stats.bayes_mvs(closure_phase)
        sigma_cp = stats_cp[2].statistic
        sigma_cp_90 = stats_cp[2].minmax[1] - sigma_cp
    except:
        sigma_cp = np.NaN
        sigma_cp_90 = np.NaN
    return sigma_cp, sigma_cp_90


class ClosurePhaseNull(object):
    def __init__(self, tpt_folder, tpt_stack_name, tiff_folder, stack_name, master_date, rml, dcoh=0.01, nrep=512):
        self.dcoh = dcoh
        self.ncohs = int(np.ceil(1 / dcoh) - 1)
        self.cohs = np.linspace(dcoh, 1 - dcoh, self.ncohs)
        self.dcoh = self.cohs[1] - self.cohs[0]
        self.nrep = nrep
        self.master_date = master_date
        self.rml = rml

        self.tpt_folder = tpt_folder
        self.tpt_stack_name = tpt_stack_name
        self.tiff_folder = tiff_folder
        self.stack_name = stack_name

        path = os.path.join(self.tiff_folder, self.stack_name, 'interferogram')
        self.proj = os.listdir(path)[0]
        self.res = int(self.proj.split('0_')[1])

        # load master date number of looks
        path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        os.chdir(path)
        filename = glob.glob(self.master_date + '*')
        nlook = xr.open_dataarray(os.path.join(path, filename[0])).data
        # load geocode
        if self.rml != 2:
            res = int(self.res * self.rml / 2)
            self.lat = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lat.nc').data
            self.lon = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lon.nc').data
            rsmp = int(self.rml / 2)
            self.nlook = drtls.smooth(drtls.smooth((self.rml/2)**2 * nlook, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:,::rsmp]
            # nlook is wrong! should be add up, not smooth!
        else:
            res = self.res
            self.nlook = nlook
            path = os.path.join(self.tiff_folder, self.stack_name, 'geocode', self.proj)
            paths = os.listdir(path)
            self.lat = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()
            self.lon = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()

        self.t_nlook = int(res ** 2 / 100)

        # load look up table
        path = self.tpt_folder + '/LUT/'

        self.sigma_cp_lut = xr.open_dataarray(
            path + str(res) + '_' + str(dcoh) + '_' + str(self.nrep) + '_sigma_cp.nc').data
        self.sigma_cp90_lut = xr.open_dataarray(
            path + str(res) + '_' + str(dcoh) + '_' + str(self.nrep) + '_sigma_cp90.nc').data

        self.tpt_dates = self.list_of_tpt_dates()

    def list_of_tpt_dates(self):
        '''
        list of triplets dates
        :return:
        '''
        tptdates = os.listdir(self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data')
        dates = []
        for i in tptdates:
            dates.append(i.split('_p')[0])
        return dates

    def load_coh(self, dates):
        '''
        load data
        :param dates:
        :param data_type:
        :return:
        '''
        if self.rml != 2:
            res = int(self.res * self.rml / 2)
            data = xr.open_dataarray(self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + dates + '_' + str(res) + '_coh.nc').data
        else:
            path = self.tiff_folder + '/' + self.stack_name + '/coherence/' + self.proj + '/'
            os.chdir(path)
            filename = glob.glob(dates + '*')
            data = gdal.Open(path + filename[0]).ReadAsArray()
        return data

    def __lut_creation(self):
        sigma_cp_lut = np.zeros([self.ncohs, self.ncohs, self.ncohs])
        sigma_cp90_lut = np.zeros([self.ncohs, self.ncohs, self.ncohs])
        # Here loop but avoid repeating combinations
        count = 0
        for i0 in range(self.ncohs):
            print("coh12=%f" % self.cohs[i0])
            for i1 in range(i0, self.ncohs):
                for i2 in range(i1, self.ncohs):
                    # print("cohs=(%f, %f, %f)" % tuple((self.cohs[[i0, i1, i2]])))
                    s_cp, s_cp_90 = cohs_to_closure_std(self.cohs[i0],
                                                        self.cohs[i1],
                                                        self.cohs[i2],
                                                        self.t_nlook,
                                                        nrep=self.nrep)
                    # This is a bit ugly. I populate all permutations
                    # not clean from a memory use perspective, but
                    # easy indexing later
                    sigma_cp_lut[i0, i1, i2] = s_cp
                    sigma_cp_lut[i0, i2, i1] = s_cp
                    sigma_cp_lut[i1, i0, i2] = s_cp
                    sigma_cp_lut[i1, i2, i0] = s_cp
                    sigma_cp_lut[i2, i0, i1] = s_cp
                    sigma_cp_lut[i2, i1, i0] = s_cp
                    sigma_cp90_lut[i0, i1, i2] = s_cp_90
                    sigma_cp90_lut[i0, i2, i1] = s_cp_90
                    sigma_cp90_lut[i1, i0, i2] = s_cp_90
                    sigma_cp90_lut[i1, i2, i0] = s_cp_90
                    sigma_cp90_lut[i2, i0, i1] = s_cp_90
                    sigma_cp90_lut[i2, i1, i0] = s_cp_90
                    count += 1
        print("Total sigma_cp calculated %i" % count)
        return sigma_cp_lut, sigma_cp90_lut

    def coh2ind(self, coh):
        if type(coh) is np.ndarray:
            ind = (np.round((coh - self.cohs[0]) / self.dcoh)).astype(int)
        else:
            ind = (np.round((np.array([coh]) - self.cohs[0]) / self.dcoh)).astype(int)
        ind = np.where(ind < 0, 0, ind)
        ind = np.where(ind > self.ncohs - 1, self.ncohs - 1, ind)
        if ind.size == 1:
            return ind[0]
        else:
            return ind

    def sigma_cp_num(self, coh12, coh23, coh13):
        ind12 = self.coh2ind(coh12)
        ind23 = self.coh2ind(coh23)
        ind13 = self.coh2ind(coh13)
        look_scaling = np.sqrt(self.t_nlook / self.nlook)
        sigma_cp = self.sigma_cp_lut[ind12, ind23, ind13] * look_scaling
        sigma_cp90 = self.sigma_cp90_lut[ind12, ind23, ind13] * look_scaling
        return sigma_cp, sigma_cp90

    def sigma_cp_asymptotic(self, coh12, coh23, coh13):
        """Computes standar deviation of closure phase.
        This implements (23) in De Zan et.al. 2015
        Parameters
        ----------
        coh12 : float or np.ndarray
            Coherence between first two images
        coh23 : float or np.ndarray
            Coherence between first two images
        coh13 : float or np.ndarray
            Coherence between first two images
        nlook : int, float, or np.dnarray
            Number of looks

        Returns
        -------
        float or np.ndarray()
            Standard deviation of closure phase under null-hypothesis.

        """
        c12_2 = np.abs(coh12) ** 2
        c23_2 = np.abs(coh23) ** 2
        c13_2 = np.abs(coh13) ** 2
        num = (3 * c12_2 * c23_2 * c13_2
               + c12_2 * c23_2
               + c23_2 * c13_2
               + c12_2 * c13_2
               - 2 * coh12 * coh23 * coh13 * (c12_2 + c23_2 + c13_2))
        den = (self.nlook * c12_2 * c23_2 * c13_2)
        var_cp = num / den
        return np.sqrt(var_cp), 0

    def sigma_cp(self, coh12, coh23, coh13, method='numerical'):
        if method == 'numerical':
            return self.sigma_cp_num(coh12, coh23, coh13)
        else:
            return self.sigma_cp_asymptotic(coh12, coh23, coh13)

    def save_result(self):
        if self.rml != 2:
            res = int(self.res * self.rml / 2)
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/significant/data/'
        else:
            res = self.res
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/significant/data/'
        for dates in self.tpt_dates:
            tri_date = dates.split('_')
            coh12 = self.load_coh(tri_date[0] + '_' + tri_date[1])
            coh23 = self.load_coh(tri_date[1] + '_' + tri_date[2])
            coh13 = self.load_coh(tri_date[0] + '_' + tri_date[2])
            sigma_cp_n = self.sigma_cp(coh12, coh23, coh13)
            sigma_cp_a = self.sigma_cp(coh12, coh23, coh13, method='asymptotic')

            sigma_cp0_n = xr.DataArray(sigma_cp_n[0])
            sigma_cp0_n.to_netcdf(path + dates + '_sigma_cp_n_' + str(res) + '.nc')

            sigma_cp90_n = xr.DataArray(sigma_cp_n[1])
            sigma_cp90_n.to_netcdf(path + dates + '_sigma_cp90_n_' + str(res) + '.nc')

            sigma_cp_a = xr.DataArray(sigma_cp_a[0])
            sigma_cp_a.to_netcdf(path + dates + '_sigma_cp_a_' + str(res) + '.nc')

    def plot_significant_ratio(self):
        if self.rml != 2:
            res = int(self.res * self.rml / 2)
            path_tpt = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/'
            path_sig = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/significant/data/'
            gif_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/significant/ani/' + str(res) + '_ratio.gif'
        else:
            res = self.res
            path_tpt = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/'
            path_sig = self.tpt_folder + '/' + self.tpt_stack_name + '/significant/data/'
            gif_path = self.tpt_folder + '/' + self.tpt_stack_name + '/significant/ani/' + str(res) + '_ratio.gif'
        image_list = []
        for dates in self.tpt_dates:
            os.chdir(path_tpt)
            filename = glob.glob(dates + '*')
            tpt_data = xr.open_dataarray(path_tpt + filename[0]).data
            sigma_cp_n = xr.open_dataarray(path_sig + dates + '_sigma_cp_n_' + str(res) + '.nc').data
            # plot figure
            plt.figure(figsize=(12, 9), dpi=300)
            plt.pcolormesh(self.lon, self.lat,
                           np.ma.masked_array(tpt_data / sigma_cp_n, np.logical_not(self.nlook != 0)),
                           cmap='bwr', vmin=-8, vmax=8)
            clb = plt.colorbar()
            clb.set_label('$\Phi_{123}/\sigma_{123}$', fontsize=15, labelpad=-35, y=1.05, rotation=0)
            plt.grid()
            plt.ylabel('Latitude [deg]', fontsize=20)
            plt.xlabel('Longitude [deg]', fontsize=20)
            plt.title(dates, fontsize=20, y=1.02)
            # save figure
            if self.rml != 2:
                img_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/significant/img/' + dates + '_' + str(
                    res) + '_ratio.png'
            else:
                img_path = self.tpt_folder + '/' + self.tpt_stack_name + '/significant/img/' + dates + '_' + str(
                    res) + '_ratio.png'
            plt.savefig(img_path, dpi=300)
            plt.close()
            image_list.append(imageio.imread(img_path))
        imageio.mimwrite(gif_path,image_list, format='GIF', duration=0.5)

# %%
if __name__ == '__main__':
    from matplotlib import pyplot as plt
    coh12 = 0.1
    coh23 = 0.6
    coh13 = 0.9
    nlook = 200
    sigma_cp, sigma_cp_90 = cohs_to_closure_std(coh12, coh23, coh13, nlook, nrep=1024)
    print("sigma_cp=%f +/- %f" % (sigma_cp, sigma_cp_90))
    cf = ClosurePhaseNull(400, dcoh=0.05)
    #%%
    # some random coherences
    coh12 = np.linspace(0.05,0.95,19).reshape((1,19))
    coh23 = np.linspace(0.05,0.95,19).reshape((19,1))
    coh13 = coh12 * coh23  # for example
    nlooks = 200
    sigma_cp_n = cf.sigma_cp(coh12, coh23, coh13, nlooks)
    sigma_cp_a = cf.sigma_cp(coh12, coh23, coh13, nlooks, method='asymptotic')
    sigma_cp_n[0].shape
    sigma_cp_a[0].shape
    plt.figure()
    plt.imshow(sigma_cp_n[0], origin='lower', vmax=np.pi/np.sqrt(3),
               extent=[0.05, 0.95,0.05,0.95])
    np.sqrt(1/3) * np.pi
    plt.colorbar()
    plt.figure()
    plt.imshow(sigma_cp_a, origin='lower', vmax=np.pi/np.sqrt(3),
               extent=[0.05, 0.95,0.05,0.95])
    plt.colorbar()
    plt.figure()
    plt.imshow(sigma_cp_n[0] - sigma_cp_a, origin='lower', vmax=1, vmin=-1,
               extent=[0.05, 0.95,0.05,0.95])
    plt.colorbar()
