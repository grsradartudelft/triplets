import os
import numpy as np
import xarray as xr
import pandas as pd

class Src_match(object):
    
    def __init__(self, tpt_folder, stack_name, win_size, geo, y_up = 36, y_low = 44, x_low = -9.5, x_up = 3.5):
        self.geometry = geo
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)
        self.tpt_folder = tpt_folder
        self.stack_name = stack_name
        self.path = '/tudelft.net/staff-umbrella/Triplets4weu/Data/Land_data/Skin_water/'
        # self.files = [f for f in os.listdir(self.path) if f.endswith('.grib')]
        self.dates = self.load_dates()
        self.y_low = y_low
        self.y_up = y_up
        self.x_low = x_low
        self.x_up = x_up
        self.lat_sar, self.lon_sar = self.load_geocode_sar()
        
    def load_geocode_sar(self):
        path=os.path.join(self.tpt_folder, self.stack_name, self.ml_proj, 'geocode', 'data')
        paths = os.listdir(path)
        lat_sar = xr.open_dataarray(os.path.join(path, paths[0])).data
        lon_sar = xr.open_dataarray(os.path.join(path, paths[1])).data
        return lat_sar, lon_sar
        
    def load_dates(self):
        nc_path = os.path.join(self.tpt_folder, self.stack_name, self.ml_proj, 'amplitude', 'data')
        dates = [i.split('_', 1)[0] for i in os.listdir(nc_path)]
        dates = pd.to_datetime(dates)
        return dates
    
    def match_data(self, date):
        # find the correponding file
        filename = date.strftime('%Y%m') +'.grib'
        # load grib data
        ds = xr.open_dataset(os.path.join(self.path, filename), engine="cfgrib")
        # find index with time and geometry
        # ascending in the afternoon: ds.time[1::2], in the evening around 18
        # descending in the morning: ds.time[::2], in the morning around 6
        if self.geometry == 'asc':
            geo_time = int(18)
        elif self.geometry == 'dsc':
            geo_time= int(6)
        match_ind = np.where((pd.to_datetime(ds.time.data).day == date.day)&(pd.to_datetime(ds.time.data).hour==geo_time))[0][0]
        # print(match_ind)
        # find corresponding src data
        src_data = ds.src[match_ind,:,:].data
        return src_data
    
    def src_sar(self, date):
        dlat = -0.1
        dlon = 0.1
        # lat_ind = np.round((self.lat_sar - self.y_up) / dlat-1).astype(int)
        # lon_ind = np.round((self.lon_sar - self.x_low) / dlon).astype(int)
        lat_ind = np.trunc((self.lat_sar - self.y_up) / dlat-2).astype(int)
        lon_ind = np.trunc((self.lon_sar - self.x_low) / dlon).astype(int)
        src_data = self.match_data(date)
        Maps_sar = src_data[lat_ind, lon_ind]
        return Maps_sar
    
    def save_data(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)
        
    def save_match(self):
        for date in self.dates:
            src_data = self.src_sar(date)
            self.save_data(src_data, os.path.join(self.tpt_folder, self.stack_name, self.ml_proj, 'src_m', date.strftime('%Y%m%d') + '_src.nc'))
            print(date)