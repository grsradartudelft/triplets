import os
import glob
import numpy as np
import xarray as xr

class Src_tpt(object):
    '''
    generating one-way baseline
    in order to get the 2-way vertical baseline (kappa) triplets
    needs to divide the end products by 8
    '''
    def __init__(self, tpt_folder, stack_name, win_size):
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)
        self.tpt_folder = os.path.join(tpt_folder, stack_name, self.ml_proj)
        self.src_path = os.path.join(self.tpt_folder,'src_m')
        self.tpt_dates = self.load_tptdates()
        
                    
    def load_tptdates(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        filenames = os.listdir(os.path.join(self.tpt_folder,'triplets', 'data'))
        tpt_dates = []
        for filename in filenames:
            tpt_dates.append(filename[:-7])
        return tpt_dates
        
    def load_src(self, tri_date):
        '''
        load data
        :param dates:
        :param data_type:
        :return:
        '''
        dates = tri_date.split('_')
        src1 = xr.open_dataarray(os.path.join(self.src_path, dates[0]+'_src.nc')).data
        src2 = xr.open_dataarray(os.path.join(self.src_path, dates[1]+'_src.nc')).data
        src3 = xr.open_dataarray(os.path.join(self.src_path, dates[2]+'_src.nc')).data
        return src1, src2, src3
    
    def model_cp(self, tri_date):
        src1, src2, src3 = self.load_src(tri_date)
        src12 = src1 - src2
        src23 = src2 - src3
        src31 = src3 - src1
        
        src_cp = src12 * src23 * src31
        return src_cp
                
    def save_src_tpt(self):
        '''
        re-multilooking data
        :param data_type:
        :return:
        '''
        for tri_date in self.tpt_dates:
            print(tri_date)
            src_tpt = self.model_cp(tri_date)
            self.save_data(src_tpt, os.path.join(self.tpt_folder, 'src_tpt', 'data', tri_date + '_src.nc'))
    
    def save_data(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)