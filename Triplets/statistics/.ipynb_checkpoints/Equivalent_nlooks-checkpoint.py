import os
import glob
import gdal
import xarray as xr
import numpy as np
import scipy.stats as stats

class Estimate_ENL(object):
    
    def __init__(self, tpt_folder, stack_name, npixels, class_map, win_size, wn_az, wn_rg, az_bw, rg_bw, fs_rg, PRF):
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)
        self.dimg = np.array([npixels.shape[0], npixels.shape[1]])
        self.tpt_folder = os.path.join(tpt_folder,stack_name, self.ml_proj)
        self.npixels = npixels
        self.class_map = class_map
        self.main_map = self.main_class()
        self.wn_az = wn_az
        self.wn_rg = wn_rg
        self.az_bw = az_bw #Hz
        self.rg_bw = rg_bw # MHz
        self.fs_rg = fs_rg # MHz
        self.prf = PRF # Hz
        self.OSR_tot = self.osr()
        self.mlpixels = self.ml_npixels()
        
    def main_class(self):
#       cite the function from the multilook class
        ndim = np.ceil(self.dimg/self.win_size)
        main_map = np.zeros(self.dimg)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_map = np.where(sub_map == stats.mode(sub_map,axis=None)[0][0], 1, 0)
                main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)] = sub_map
        return main_map
        
    def ml_npixels(self):
        '''
        number of looks in RIPPL outputs should be modified
        new number of looks is saved as netcdf in tpt folder
        :return:
        '''
        ndim = np.ceil(self.dimg/self.win_size).astype(int)
        ml_npixels = np.zeros(ndim)
        
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_npixel = self.npixels[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                ml_npixels[ii,jj] = np.sum(sub_npixel[sub_map==1])
        return ml_npixels
        
    def osr(self):
        # compute oversampling factor to modify number of looks
        # oversampling rates in each direction
        OSR_rg = self.fs_rg/self.rg_bw
        OSR_az = self.prf/self.az_bw
        
        # total oversampling ratio
        OSR_tot = OSR_rg * OSR_az
        return OSR_tot
    
    def factorization(self, num):
        for i in np.arange(num-14):
            k = i + 15
            if num % k == 0:
                return int(num/k), k
            else:
                return int(np.ceil(num/16)), 16
            
    def h_win(self, alpha, n):
        '''
        Hann window: alpha = 0.5
        Hamming window: alpha = 0.54
        '''
        num = np.arange(0,n)
        wn = alpha - (1-alpha)*np.cos(2 * np.pi * num / (n-1))
        return wn
    
    
    def zp_win(self, wn):
        ''' 
        Zero-padding
        '''
        num = wn.shape[0]
        wz = np.concatenate((np.zeros(int(num/2)), wn, np.zeros(int(num/2))),axis=0)
        wz /= np.sqrt(np.sum(wz**2))
        wz *= np.sqrt(wz.shape[0])
        return wz
    
    def bw_eff(self,npixel):
        # estimate bandwidth efficiency
        factors = self.factorization(npixel)
        w0 = self.zp_win(np.ones(int(factors[0])))
        w1 = self.zp_win(self.h_win(self.wn_az, int(factors[0])))
        coe_az = (np.sum(w1**2)**2 / np.sum(w1**4))/(np.sum(w0**2)**2 / np.sum(w0**4))
        w0 = self.zp_win(np.ones(int(factors[1])))
        w1 = self.zp_win(self.h_win(self.wn_rg, int(factors[1])))
        coe_rg = (np.sum(w1**2)**2 / np.sum(w1**4))/(np.sum(w0**2)**2 / np.sum(w0**4))
        return coe_az, coe_rg
    
    def cor_shape(self, dataset):
        dim = np.floor(self.dimg/self.win_size)
        if dataset.shape[0]>dim[0]:
            dataset = dataset[:-1,:]
        if dataset.shape[1]>dim[1]:
            dataset = dataset[:,:-1]
        return dataset
    
    def cor_nlook(self):
        enl = np.zeros_like(self.mlpixels)
        for ii in np.arange(enl.shape[0]):
            for jj in np.arange(enl.shape[1]):
                if self.mlpixels[ii,jj] < 15:
                    enl[ii,jj] = np.NaN
                else:
                    coe_az, coe_rg = self.bw_eff(self.mlpixels[ii,jj])
                    enl[ii,jj] = self.mlpixels[ii,jj]/(self.OSR_tot/(coe_az * coe_rg))
        enl = self.cor_shape(enl)
        return enl
    
    def save_data(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)