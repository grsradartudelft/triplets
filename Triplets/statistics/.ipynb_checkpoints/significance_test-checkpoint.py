import os
import glob
import xarray as xr
import numpy as np

class Sigma_Null(object):
    def __init__(self, tpt_folder, stack_name, win_size, dcoh=0.01):
        self.dcoh = dcoh
        self.ncohs = int(np.ceil(1 / dcoh) - 1)
        self.cohs = np.linspace(dcoh, 1 - dcoh, self.ncohs)
        self.dcoh = self.cohs[1] - self.cohs[0]
        
        self.ml_proj = str(win_size * 20)
        self.tpt_folder = os.path.join(tpt_folder,stack_name, self.ml_proj)

        self.t_nlook = 2**11
        self.enl = xr.open_dataarray(os.path.join(self.tpt_folder, 'number_of_looks', 'data', 'ENL.nc')).data

        self.sigma_cp_lut = xr.open_dataarray('/tudelft.net/staff-umbrella/Triplets/Data/Application/Significance_para/lut_2048.nc').data

        self.tpt_dates = self.list_of_tpt_dates()

    def list_of_tpt_dates(self):
        '''
        list of triplets dates
        :return:
        '''
        tptdates = os.listdir(os.path.join(self.tpt_folder, 'triplets', 'data'))
        dates = []
        for i in tptdates:
            dates.append(i.split('_t')[0])
        return dates

    def load_coh(self, dates):
        '''
        load data
        :param dates:
        :param data_type:
        :return:
        '''
        path = os.path.join(self.tpt_folder, 'coherence', 'data')
        os.chdir(path)
        filename = glob.glob(dates + '*')
        data = xr.open_dataarray(os.path.join(path, filename[0])).data
        return data

    def coh2ind(self, coh):
        if type(coh) is np.ndarray:
            ind = (np.round((coh - self.cohs[0]) / self.dcoh)).astype(int)
        else:
            ind = (np.round((np.array([coh]) - self.cohs[0]) / self.dcoh)).astype(int)
        ind = np.where(ind < 0, 0, ind)
        ind = np.where(ind > self.ncohs - 1, self.ncohs - 1, ind)
        if ind.size == 1:
            return ind[0]
        else:
            return ind

    def sigma_cp(self, coh12, coh23, coh13):
        ind12 = self.coh2ind(coh12)
        ind23 = self.coh2ind(coh23)
        ind13 = self.coh2ind(coh13)
        look_scaling = np.sqrt(self.t_nlook / self.enl)
        sigma_cp = self.sigma_cp_lut[ind12, ind23, ind13] * look_scaling
        return sigma_cp

    def save_result(self):
        for dates in self.tpt_dates:
            tri_date = dates.split('_')
            coh12 = self.load_coh(tri_date[0] + '_' + tri_date[1])
            coh23 = self.load_coh(tri_date[1] + '_' + tri_date[2])
            coh13 = self.load_coh(tri_date[0] + '_' + tri_date[2])
            sigma_cp_n = self.sigma_cp(coh12, coh23, coh13)

            sigma_cp0_n = xr.DataArray(sigma_cp_n)
            sigma_cp0_n.to_netcdf(os.path.join(self.tpt_folder, 'significant', 'data', dates + '_sig.nc'))