import os

def folders(main_folder, stack_name, ml_proj):
    '''
    set up forders for triplets datesets and figures
    for each data stack, RUN ONCE ONLY
    :param main_folder:
    :param stack_name:
    :return:
    '''

    folder_type = ['data', 'img', 'ani']

    data_type = ['geocode', 'triplets', 'coherence', 'number_of_looks', 'statistics', 'amplitude', 'significant', 'classify_map', 'height_to_phase', 'src_tpt']

    # make folder for processing results
    
    tpt_folder = os.path.join(main_folder,stack_name, ml_proj)
    
    os.makedirs(tpt_folder)

    for dtype in data_type:
        for ftype in folder_type:
            os.makedirs(os.path.join(tpt_folder, dtype, ftype))
