import os
import glob
import imageio
import numpy as np
import xarray as xr
from osgeo import gdal
import drama.utils as drtls
import matplotlib.pyplot as plt

class tpt_formation(object):

    def __init__(self, tpt_folder, tpt_stack_name, tiff_folder, stack_name, pol, rml):

        self.tpt_folder = tpt_folder
        self.tpt_stack_name = tpt_stack_name
        self.tiff_folder = tiff_folder
        self.stack_name = stack_name

        path = tiff_folder + stack_name + '/' + 'interferogram/'
        self.proj = os.listdir(path)[0]
        self.res = int(self.proj.split('0_')[1])

        self.pol = pol
        self.rml = rml

        self.date_pairs = self.list_of_date_pair()

    def list_of_date_pair(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        path = self.tiff_folder + self.stack_name + '/' + 'interferogram/' + self.proj + '/'
        ifg_dates = os.listdir(path)
        dates = []
        for i in ifg_dates:
            dates.append(i.split('_int')[0])
        return dates

    def list_of_tpt_dates(self):
        '''
        list of triplets dates
        :return:
        '''
        tptdates = os.listdir(self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/')
        dates = []
        for i in tptdates:
            if i.endswith('phase.nc'):
                dates.append(i.split('_p')[0])
        return dates

    def ifg_form(self, datepair, tpt_formation=True):
        '''
        As RIPPL interferogram outputs first band amplitudes are not calibrated,
        amplitudes for interferogram are recomputed,
        and the outputs are formed as phasors for multilooking process
        :param datepair:
        :param tpt_formation:
        :return:
        '''
        #     compute interferogram amplitudes
        dates = datepair.split('_')
        path = self.tiff_folder + self.stack_name + '/' + 'calibrated_amplitude/' + self.proj + '/'
        os.chdir(path)
        filename = glob.glob(dates[0] + '_c' + '*')
        camp1 = gdal.Open(path + filename[0]).ReadAsArray()
        camp1 = 10 ** (camp1 / 10)
        filename = glob.glob(dates[1] + '_c' + '*')
        camp2 = gdal.Open(path + filename[0]).ReadAsArray()
        camp2 = 10 ** (camp2 / 10)
        path = self.tiff_folder + self.stack_name + '/' + 'coherence/' + self.proj + '/'
        os.chdir(path)
        filename = glob.glob(datepair + '*')
        coh = gdal.Open(path + filename[0]).ReadAsArray()
        amp = coh * np.sqrt(camp1 ** 2 * camp2 ** 2)
        path = self.tiff_folder + self.stack_name + '/' + 'interferogram/' + self.proj + '/'
        os.chdir(path)
        filename = glob.glob(datepair + '*')
        phi = gdal.Open(path + filename[0])
        phi = phi.GetRasterBand(2).ReadAsArray()
        if tpt_formation:
            return amp * np.exp(1j * phi)
        else:
            return camp1, camp2, coh * np.exp(1j * phi) * camp1 * camp2

    def create_tpt(self):
        '''
        Formation of triplets,
        the outputs are stored as netcdf for large dataset processing
        :return:
        '''
        ind = np.linspace(0, len(self.date_pairs) - 3, int((len(self.date_pairs) - 1) / 2))
        num = 1
        for i in ind:
            i = int(i)
            print('Forming triplets ' + str(num) + ' out of ' + str(int((len(self.date_pairs) - 1) / 2)))
            ifg12 = self.ifg_form(self.date_pairs[i])
            ifg13 = self.ifg_form(self.date_pairs[i + 1])
            ifg23 = self.ifg_form(self.date_pairs[i + 2])
            ifg = ifg12 * ifg23 * np.conj(ifg13)
            file = self.date_pairs[i] + '_' + self.date_pairs[i + 2].split('_')[1]
            X_phi = xr.DataArray(np.angle(ifg))
            path_phi = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/' + file + '_phase.nc'
            X_phi.to_netcdf(path_phi)
            num = num + 1
        self.tpt_dates = self.list_of_tpt_dates()

    def modify_number_of_looks(self):
        '''
        number of looks in RIPPL outputs should be modified
        new number of looks is saved as netcdf in tpt folder
        :return:
        '''
        path = self.tiff_folder + self.stack_name + '/' + 'calibrated_amplitude/' + self.proj + '/'
        os.chdir(path)
        filenames = glob.glob('*no_of_looks_' + self.pol + '@' + self.proj + '_in_coor_radar.tiff')
        # compute mean number of looks
        e_NoL = self.res ** 2 / 100
        for filename in filenames:
            NoL = gdal.Open(path + filename).ReadAsArray()
            NoL = e_NoL * NoL / np.mean(NoL)
            X_NoL = xr.DataArray(NoL)
            path_NoL = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/' + filename[0:8] + '.nc'
            X_NoL.to_netcdf(path_NoL)

    def multilooking(self, dates, data_type=''):
        '''
        re-multilooking data
        :param data_type:
        :return:
        '''
        rsmp = int(self.rml / 2)
        dates = dates.split('_')

        if data_type == 'geocode':
            path = self.tiff_folder + self.stack_name + '/' + 'geocode/' + self.proj + '/'
            paths = os.listdir(path)
            lat = gdal.Open(path + paths[0]).ReadAsArray()
            lon = gdal.Open(path + paths[1]).ReadAsArray()
            lat = drtls.smooth(drtls.smooth(lat, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
            lon = drtls.smooth(drtls.smooth(lon, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
            return lat, lon

        elif data_type == 'triplets':
            amp1, amp2, coh12 = self.ifg_form(dates[0] + '_' + dates[1], tpt_formation=False)
            coh12 = drtls.smooth(drtls.smooth(coh12, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
            amp1 = np.sqrt(
                drtls.smooth(drtls.smooth(amp1 ** 2, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp])
            amp2 = np.sqrt(
                drtls.smooth(drtls.smooth(amp2 ** 2, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp])
            coh12 = coh12 / (amp1 * amp2)
            amp12 = np.abs(coh12 * np.sqrt(amp1 ** 2 * amp2 ** 2))
            ifg12 = amp12 * np.exp(1j * np.angle(coh12))

            amp3, coh23 = self.ifg_form(dates[1] + '_' + dates[2], tpt_formation=False)[1:3]
            coh23 = drtls.smooth(drtls.smooth(coh23, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
            amp3 = np.sqrt(
                drtls.smooth(drtls.smooth(amp3 ** 2, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp])
            coh23 = coh23 / (amp2 * amp3)
            amp23 = np.abs(coh23 * np.sqrt(amp2 ** 2 * amp3 ** 2))
            ifg23 = amp23 * np.exp(1j * np.angle(coh23))

            coh13 = self.ifg_form(dates[0] + '_' + dates[2], tpt_formation=False)[2]
            coh13 = drtls.smooth(drtls.smooth(coh13, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
            coh13 = coh13 / (amp1 * amp3)
            amp13 = np.abs(coh13 * np.sqrt(amp1 ** 2 * amp3 ** 2))
            ifg13 = amp13 * np.exp(1j * np.angle(coh13))

            tpt = ifg12 * ifg23 * np.conj(ifg13)

            return amp1, amp2, amp3, coh12, coh23, coh13, tpt

    def save_ml(self):
        self.tpt_dates = self.list_of_tpt_dates()
        #    compute resolution
        res = int(self.res * self.rml / 2)  # resolution
        lat, lon = self.multilooking(self.tpt_dates[0], data_type='geocode')
        # save geocode data
        lat = xr.DataArray(lat)
        path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lat.nc'
        lat.to_netcdf(path)

        lon = xr.DataArray(lon)
        path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lon.nc'
        lon.to_netcdf(path)

        for dates in self.tpt_dates:
            amp1, amp2, amp3, coh12, coh23, coh13, tpt = self.multilooking(dates, data_type='triplets')
            three_dates = dates.split('_')

            # save amplitude data
            amp1 = xr.DataArray(amp1)
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + three_dates[0]  + '_' + str(res)+ '_amp.nc'
            amp1.to_netcdf(path)

            amp2 = xr.DataArray(amp2)
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + three_dates[
                1] + '_' + str(res) + '_amp.nc'
            amp2.to_netcdf(path)

            amp3 = xr.DataArray(amp3)
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + three_dates[
                2] + '_' + str(res) + '_amp.nc'
            amp3.to_netcdf(path)

            # save coherence data
            coh12 = xr.DataArray(np.abs(coh12))
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + three_dates[
                0] + '_' + three_dates[1] + '_' + str(res) + '_coh.nc'
            coh12.to_netcdf(path)

            coh23 = xr.DataArray(np.abs(coh23))
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + three_dates[
                1] + '_' + three_dates[2] + '_' + str(res) + '_coh.nc'
            coh23.to_netcdf(path)

            coh13 = xr.DataArray(np.abs(coh13))
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + three_dates[
                0] + '_' + three_dates[2] + '_' + str(res) + '_coh.nc'
            coh13.to_netcdf(path)

            # save triplets data
            tpt = xr.DataArray(np.angle(tpt))
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/' + dates + '_' + str(res) + '_phase.nc'
            tpt.to_netcdf(path)

    def plot_ml(self, lat, lon, dates):
        '''
        plot re-multilooked data
        :param dates:
        :return:
        '''
        #    compute resolution
        res = int(self.res * self.rml / 2)  # resolution

        #    load multilooking data
        # triplets
        tpt = xr.open_dataarray(self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/' + dates + '_' +  str(res) + '_phase.nc').data

        # mask data
        # define the first dataset as the mask for all stack and resample it
        rsmp = int(self.rml / 2)
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        nlooks_mask = drtls.smooth(drtls.smooth(nlooks_mask, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:, ::rsmp]
        tpt_mask = np.ma.masked_array(tpt, np.logical_not(nlooks_mask != 0))

        #  plot tpt phase
        plt.figure(figsize=(12, 9), dpi=300)
        plt.pcolormesh(lon, lat, np.degrees(tpt_mask), cmap='bwr', vmin=-60, vmax=60)
        clb = plt.colorbar()
        clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
        plt.grid()
        plt.ylabel('Latitude [deg]', fontsize=20)
        plt.xlabel('Longitude [deg]', fontsize=20)
        plt.title(dates, fontsize=20, y=1.02)
        plt.savefig(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/img/' + dates + '_' + str(res) + '_phase.png',
            dpi=300)
        plt.close()

        dates = dates.split('_')

        # amplitudes
        amp1 = xr.open_dataarray(self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + dates[0]  + '_' + str(res)+ '_amp.nc').data
        amp2 = xr.open_dataarray(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + dates[1] + '_' + str(
                res) + '_amp.nc').data
        amp3 = xr.open_dataarray(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/data/' + dates[2] + '_' + str(
                res) + '_amp.nc').data

        #  plot amplitude
        fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
        im1 = ax[0].pcolormesh(lon, lat, 10 * np.log10(amp1), vmax=0, vmin=-30, cmap='bone')
        im2 = ax[1].pcolormesh(lon, lat, 10 * np.log10(amp2), vmax=0, vmin=-30, cmap='bone')
        im3 = ax[2].pcolormesh(lon, lat, 10 * np.log10(amp3), vmax=0, vmin=-30, cmap='bone')
        ax[0].grid()
        ax[0].set_title(dates[0], fontsize=20, y=1.02)
        ax[0].set_ylabel('Latitude [deg]', fontsize=20)
        ax[0].set_xlabel('Longitude [deg]', fontsize=20)
        ax[1].grid()
        ax[1].set_title(dates[1], fontsize=20, y=1.02)
        ax[1].set_ylabel('Latitude [deg]', fontsize=20)
        ax[1].set_xlabel('Longitude [deg]', fontsize=20)
        ax[2].grid()
        ax[2].set_title(dates[2], fontsize=20, y=1.02)
        ax[2].set_ylabel('Latitude [deg]', fontsize=20)
        ax[2].set_xlabel('Longitude [deg]', fontsize=20)
        fig.subplots_adjust(right=0.85)
        cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
        clb = fig.colorbar(im3, cax=cbar_ax)
        clb.set_label('$\sigma_0$ [dB]', fontsize=15, labelpad=-32, y=1.07, rotation=0)
        plt.savefig(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/img/' + dates[0] + '_' + dates[1] + '_' + dates[
                2] + '_' + str(res) + '_amp.png', dpi=300)
        plt.close()

        # coherence
        coh12 = xr.open_dataarray(self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + dates[
                0] + '_' + dates[1] + '_' + str(res) + '_coh.nc').data
        coh23 = xr.open_dataarray(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + dates[
                1] + '_' + dates[2] + '_' + str(res) + '_coh.nc').data
        coh13 = xr.open_dataarray(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/data/' + dates[
                0] + '_' + dates[2] + '_' + str(res) + '_coh.nc').data

        # mask coherence
        coh12 = np.ma.masked_array(coh12, np.logical_not(nlooks_mask != 0))
        coh23 = np.ma.masked_array(coh23, np.logical_not(nlooks_mask != 0))
        coh13 = np.ma.masked_array(coh13, np.logical_not(nlooks_mask != 0))

        #  plot coherence
        fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
        im1 = ax[0].pcolormesh(lon, lat, coh12, vmax=1, vmin=0, cmap='bone')
        im2 = ax[1].pcolormesh(lon, lat, coh23, vmax=1, vmin=0, cmap='bone')
        im3 = ax[2].pcolormesh(lon, lat, coh13, vmax=1, vmin=0, cmap='bone')
        ax[0].grid()
        ax[0].set_title(dates[0] + '_' + dates[1], fontsize=20, y=1.02)
        ax[0].set_ylabel('Latitude [deg]', fontsize=20)
        ax[0].set_xlabel('Longitude [deg]', fontsize=20)
        ax[1].grid()
        ax[1].set_title(dates[1] + '_' + dates[2], fontsize=20, y=1.02)
        ax[1].set_ylabel('Latitude [deg]', fontsize=20)
        ax[1].set_xlabel('Longitude [deg]', fontsize=20)
        ax[2].grid()
        ax[2].set_title(dates[0] + '_' + dates[2], fontsize=20, y=1.02)
        ax[2].set_ylabel('Latitude [deg]', fontsize=20)
        ax[2].set_xlabel('Longitude [deg]', fontsize=20)
        fig.subplots_adjust(right=0.85)
        cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
        clb = fig.colorbar(im3, cax=cbar_ax)
        clb.set_label('$\gamma$', fontsize=15, labelpad=-32, y=1.07, rotation=0)
        plt.savefig(
            self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/img/' + dates[0] + '_' + dates[1] + '_' + dates[
                2] + '_' + str(res) + '_coh.png', dpi=300)
        plt.close()

    def plot_time_series(self, multilook=False):
        '''
        plot time series
        :param multilook:
        :return:
        '''
        self.tpt_dates = self.list_of_tpt_dates()
        if self.rml != 2:
            multilook = True
            res = int(self.res * self.rml / 2)
            lat_ml = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lat.nc').data
            lon_ml = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lon.nc').data
            
        # define the first dataset as the mask for all stack
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        #    set_up_figures
        fig, ax = plt.subplots(figsize=(12, 9), dpi=300)
        for dates in self.tpt_dates:
            #         load tpt_phase
            if multilook:
                self.plot_ml(lat_ml, lon_ml, dates)
            else:
                # load geocode
                path = self.tiff_folder + self.stack_name + '/' + 'geocode/' + self.proj + '/'
                paths = os.listdir(path)
                lat = gdal.Open(path + paths[0]).ReadAsArray()
                lon = gdal.Open(path + paths[1]).ReadAsArray()

                # load tpt data
                os.chdir(self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/')
                filename = glob.glob(dates + '_p' + '*')

                tpt_phase = xr.open_dataarray(
                    self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/' + filename[0]).data
                tpt_phase_mask = np.ma.masked_array(tpt_phase, np.logical_not(nlooks_mask != 0))

                # plot tpt phase
                plt.figure(figsize=(12, 9))
                plt.pcolormesh(lon, lat, np.degrees(tpt_phase_mask), cmap='bwr', vmin=-60, vmax=60)
                clb = plt.colorbar()
                clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
                plt.grid()
                plt.ylabel('Latitude [deg]', fontsize=20)
                plt.xlabel('Longitude [deg]', fontsize=20)
                plt.title(dates, fontsize=20, y=1.02)
                plt.savefig(self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/img/' + dates + '_' + str(
                    self.res) + '_phase.png', dpi=300)
                plt.close()

                # load amplitude
                path = self.tiff_folder + self.stack_name + '/' + 'calibrated_amplitude/' + self.proj + '/'
                os.chdir(path)
                coh_dates = dates.split('_')
                filename = glob.glob(dates.split('_')[0] + '*')
                amp1 = gdal.Open(path + filename[0]).ReadAsArray()
                filename = glob.glob(dates.split('_')[1] + '*')
                amp2 = gdal.Open(path + filename[0]).ReadAsArray()
                filename = glob.glob(dates.split('_')[2] + '*')
                amp3 = gdal.Open(path + filename[0]).ReadAsArray()

                # plot amplitude
                fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
                im1 = ax[0].pcolormesh(lon, lat, amp1, vmax=0, vmin=-30, cmap='bone')
                im2 = ax[1].pcolormesh(lon, lat, amp2, vmax=0, vmin=-30, cmap='bone')
                im3 = ax[2].pcolormesh(lon, lat, amp3, vmax=0, vmin=-30, cmap='bone')
                ax[0].grid()
                ax[0].set_title(dates.split('_')[0], fontsize=20, y=1.02)
                ax[0].set_ylabel('Latitude [deg]', fontsize=20)
                ax[0].set_xlabel('Longitude [deg]', fontsize=20)
                ax[1].grid()
                ax[1].set_title(dates.split('_')[1], fontsize=20, y=1.02)
                ax[1].set_ylabel('Latitude [deg]', fontsize=20)
                ax[1].set_xlabel('Longitude [deg]', fontsize=20)
                ax[2].grid()
                ax[2].set_title(dates.split('_')[2], fontsize=20, y=1.02)
                ax[2].set_ylabel('Latitude [deg]', fontsize=20)
                ax[2].set_xlabel('Longitude [deg]', fontsize=20)
                fig.subplots_adjust(right=0.85)
                cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
                clb = fig.colorbar(im3, cax=cbar_ax)
                clb.set_label('$\sigma_0$ [dB]', fontsize=15, labelpad=-32, y=1.07, rotation=0)
                plt.savefig(self.tpt_folder + '/' + self.tpt_stack_name + '/amplitude/img/' + dates + '_' + str(
                    self.res) + '_amp.png', dpi=300)
                plt.close()

                # load coherence
                path = self.tiff_folder + self.stack_name + '/' + 'coherence/' + self.proj + '/'
                os.chdir(path)
                coh_dates = dates.split('_')
                filename = glob.glob(coh_dates[0] + '_' + coh_dates[1] + '*')
                coh12 = gdal.Open(path + filename[0]).ReadAsArray()
                filename = glob.glob(coh_dates[1] + '_' + coh_dates[2] + '*')
                coh23 = gdal.Open(path + filename[0]).ReadAsArray()
                filename = glob.glob(coh_dates[0] + '_' + coh_dates[2] + '*')
                coh13 = gdal.Open(path + filename[0]).ReadAsArray()

                # mask coherence
                coh12 = np.ma.masked_array(coh12, np.logical_not(nlooks_mask != 0))
                coh23 = np.ma.masked_array(coh23, np.logical_not(nlooks_mask != 0))
                coh13 = np.ma.masked_array(coh13, np.logical_not(nlooks_mask != 0))

                # plot coherence
                fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
                im1 = ax[0].pcolormesh(lon, lat, coh12, vmax=1, vmin=0, cmap='bone')
                im2 = ax[1].pcolormesh(lon, lat, coh23, vmax=1, vmin=0, cmap='bone')
                im3 = ax[2].pcolormesh(lon, lat, coh13, vmax=1, vmin=0, cmap='bone')
                ax[0].grid()
                ax[0].set_title(coh_dates[0] + '_' + coh_dates[1], fontsize=20, y=1.02)
                ax[0].set_ylabel('Latitude [deg]', fontsize=20)
                ax[0].set_xlabel('Longitude [deg]', fontsize=20)
                ax[1].grid()
                ax[1].set_title(coh_dates[1] + '_' + coh_dates[2], fontsize=20, y=1.02)
                ax[1].set_ylabel('Latitude [deg]', fontsize=20)
                ax[1].set_xlabel('Longitude [deg]', fontsize=20)
                ax[2].grid()
                ax[2].set_title(coh_dates[0] + '_' + coh_dates[2], fontsize=20, y=1.02)
                ax[2].set_ylabel('Latitude [deg]', fontsize=20)
                ax[2].set_xlabel('Longitude [deg]', fontsize=20)
                fig.subplots_adjust(right=0.85)
                cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
                clb = fig.colorbar(im3, cax=cbar_ax)
                clb.set_label('$\gamma$', fontsize=15, labelpad=-32, y=1.07, rotation=0)
                plt.savefig(self.tpt_folder + '/' + self.tpt_stack_name + '/coherence/img/' + dates + '_' + str(
                    self.res) + '_coh.png', dpi=300)
                plt.close()

    def animate_from_images(self):
        '''
        animation from images
        :return:
        '''

        if self.rml != 2:
            res = int(self.res * self.rml / 2)  # resolution
            image_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/img/'
            gif_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/ani/' + str(res) + '_amp.gif'
            image_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/img/'
            gif_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/ani/' + str(res) + '_phase.gif'
            image_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/img/'
            gif_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/ani/' + str(res) + '_coh.gif'
        else:
            res = self.res
            image_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/amplitude/img/'
            gif_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/amplitude/ani/' + str(res) + '_amp.gif'
            image_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/img/'
            gif_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/ani/' + str(res) + '_phase.gif'
            image_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/coherence/img/'
            gif_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/coherence/ani/' + str(res) + '_coh.gif'

        # animate amplitude
        os.chdir(image_path_amp)

        filenames = glob.glob('*' + str(res) + '_amp.png')
        image_list = []
        for filename in filenames:
            image_list.append(imageio.imread(image_path_amp + filename))
        imageio.mimwrite(gif_path_amp, image_list, format='GIF', duration=0.5)

        # animate phase
        os.chdir(image_path_phase)

        filenames = glob.glob('*' + str(res) + '_phase.png')
        image_list = []
        for filename in filenames:
            image_list.append(imageio.imread(image_path_phase + filename))
        imageio.mimwrite(gif_path_phase, image_list, format='GIF', duration=0.5)

        # animate coherence
        os.chdir(image_path_coh)

        filenames = glob.glob('*' + str(res) + '_coh.png')
        image_list = []
        for filename in filenames:
            image_list.append(imageio.imread(image_path_coh + filename))
        imageio.mimwrite(gif_path_coh, image_list, format='GIF', duration=0.5)


