import os
import glob
import numpy as np
import xarray as xr
import pandas as pd
from osgeo import gdal
import scipy.stats as stats
import drama.utils as drtls

class Adapt_ML(object):
    def __init__(self, tiff_folder, tpt_folder, stack_name, win_size, pol, proj, class_map, slice_index):
        self.pol = pol
        self.proj = proj
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)
        self.dimg = np.array([class_map.shape[0], class_map.shape[1]])
        self.slice = slice_index
        self.class_map = class_map
        self.main_map = self.main_class()
        
        self.tiff_folder = os.path.join(tiff_folder,stack_name)
        self.tpt_folder = os.path.join(tpt_folder,stack_name, self.ml_proj)
        self.tpt_dates = self.list_of_tptdates()
                
    def list_of_tptdates(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        path = os.path.join(self.tiff_folder, 'calibrated_amplitude', self.proj)
        os.chdir(path)
        camp = 'db_' + self.pol + '@' + self.proj + '_in_coor_radar.tiff'
        camp_filenames = glob.glob('*' + camp)
        dates = []
        for ii in camp_filenames:
            dates.append(ii.split('_cal')[0])
        # creat dates triplets 
        dates = sorted(dates)
        ind = np.arange(len(dates)-2)
        tpt_dates = []
        for ii in ind:
            day12 = (pd.to_datetime(dates[ii+1]).date()-pd.to_datetime(dates[ii]).date()).days
            day23 = (pd.to_datetime(dates[ii+2]).date()-pd.to_datetime(dates[ii+1]).date()).days
            if np.logical_and(day12==6, day23==6):
                tpt_dates.append(dates[ii] + '_'+ dates[ii+1]+ '_'+ dates[ii+2])
        return tpt_dates
        
    def main_class(self):
        ndim = np.ceil(self.dimg/self.win_size)
        main_map = np.zeros(self.dimg)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_map = np.where(sub_map == stats.mode(sub_map,axis=None)[0][0], 1, 0)
                main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)] = sub_map
        return main_map
    
    def ml_geo(self):
        '''
        re-multilooking data
        :param data_type:
        :return:
        '''
        path = os.path.join(self.tiff_folder, 'geocode', self.proj)
        paths = os.listdir(path)
        lat = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        lon = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        lat = drtls.smooth(lat,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
        lon = drtls.smooth(lon,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
        return lat, lon
    
    def multilooking(self, tptdates):
        dates = tptdates.split('_')
        
        # read in amplitude to construct phasor
        nc_path = os.path.join(self.tiff_folder, 'camp_nc')
        os.chdir(nc_path)
        filename = glob.glob(dates[0] + '*')
        amp1 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        filename = glob.glob(dates[1] + '*')
        amp2 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        filename = glob.glob(dates[2] + '*')
        amp3 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        
        # read in coherence data
        path = os.path.join(self.tiff_folder, 'coherence', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        coh12 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        coh23 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        coh13 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        
        # read in inteferometric phase
        path = os.path.join(self.tiff_folder, 'interferogram/', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        phi12 = gdal.Open(os.path.join(path, filename[0]))
        phi12 = phi12.GetRasterBand(2).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        phi23 = gdal.Open(os.path.join(path, filename[0]))
        phi23 = phi23.GetRasterBand(2).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        phi13 = gdal.Open(os.path.join(path, filename[0]))
        phi13 = phi13.GetRasterBand(2).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        
        # construct phasor
        coh12 = coh12 * np.exp(1j * phi12) * amp1 * amp2
        coh23 = coh23 * np.exp(1j * phi23) * amp2 * amp3
        coh13 = coh13 * np.exp(1j * phi13) * amp1 * amp3
        
        # multilooking
        ndim = np.ceil(self.dimg/self.win_size).astype(int)
        ml_amp1 = np.zeros(ndim)
        ml_amp2 = np.zeros(ndim)
        ml_amp3 = np.zeros(ndim)
        ml_coh12 = np.zeros(ndim).astype(complex)
        ml_coh23 = np.zeros(ndim).astype(complex)
        ml_coh13 = np.zeros(ndim).astype(complex)
        
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                
                sub_amp1 = amp1[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_amp2 = amp2[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_amp3 = amp3[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                
                sub_coh12 = coh12[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_coh23 = coh23[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_coh13 = coh13[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                
                # ml_amp1[ii,jj] = np.sqrt(np.mean(np.abs(sub_amp1[sub_map==1])**2))
                # ml_amp2[ii,jj] = np.sqrt(np.mean(np.abs(sub_amp2[sub_map==1])**2))
                # ml_amp3[ii,jj] = np.sqrt(np.mean(np.abs(sub_amp3[sub_map==1])**2))
                ml_amp1[ii,jj] = np.sqrt(np.mean(sub_amp1[sub_map==1]**2))
                ml_amp2[ii,jj] = np.sqrt(np.mean(sub_amp2[sub_map==1]**2))
                ml_amp3[ii,jj] = np.sqrt(np.mean(sub_amp3[sub_map==1]**2))
                
                ml_coh12[ii,jj] = np.mean(sub_coh12[sub_map==1])
                ml_coh23[ii,jj] = np.mean(sub_coh23[sub_map==1])
                ml_coh13[ii,jj] = np.mean(sub_coh13[sub_map==1])
        
        # scale 
        ml_coh12 = ml_coh12/(ml_amp1*ml_amp2)
        ml_coh23 = ml_coh23/(ml_amp2*ml_amp3)
        ml_coh13 = ml_coh13/(ml_amp1*ml_amp3)
        
        return ml_amp1, ml_amp2, ml_amp3, ml_coh12, ml_coh23, ml_coh13
    
    def cor_shape(self, dataset):
        dim = np.floor(self.dimg/self.win_size)
        if dataset.shape[0]>dim[0]:
            dataset = dataset[:-1,:]
        if dataset.shape[1]>dim[1]:
            dataset = dataset[:,:-1]
        return dataset
    
    def save_data(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)
    
    def save_ml_data(self):
        
        # multilook geocode
        lat, lon = self.ml_geo()
        # correct shape
        lat = self.cor_shape(lat)
        lon = self.cor_shape(lon)
        # save geocode data
        self.save_data(lat, os.path.join(self.tpt_folder, 'geocode', 'data', 'lat.nc'))
        self.save_data(lon, os.path.join(self.tpt_folder, 'geocode', 'data', 'lon.nc'))

        for dates in self.tpt_dates:
            amp1, amp2, amp3, coh12, coh23, coh13 = self.multilooking(dates)
            three_dates = dates.split('_')
            
            # correct shape
            amp1 = self.cor_shape(amp1)
            amp2 = self.cor_shape(amp2)
            amp3 = self.cor_shape(amp3)
            coh12 = self.cor_shape(coh12)
            coh23 = self.cor_shape(coh23)
            coh13 = self.cor_shape(coh13)

            # save amplitude data
            self.save_data(amp1, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[0] + '_amp.nc'))
            self.save_data(amp2, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[1] + '_amp.nc'))
            self.save_data(amp3, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[2] + '_amp.nc'))

            # save coherence data
            self.save_data(np.abs(coh12), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[1] + '_coh.nc'))
            self.save_data(np.abs(coh23), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[1] + '_' + three_dates[2] + '_coh.nc'))
            self.save_data(np.abs(coh13), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[2] + '_coh.nc'))

            # save triplets data
            ifg12 = coh12*amp1*amp2
            ifg23 = coh23*amp2*amp3
            ifg13 = coh13*amp1*amp3
            tpt = np.angle(ifg12 * ifg23 * np.conj(ifg13))
            self.save_data(tpt, os.path.join(self.tpt_folder, 'triplets', 'data', dates + '_tpt.nc'))
            print(dates)