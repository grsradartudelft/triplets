import os
import glob
import numpy as np
import xarray as xr
from osgeo import gdal
import scipy.stats as stats

#  RIPPL calibrated amplitudes are actually intensities in dB


tiff_folder = '/tudelft.net/staff-umbrella/Triplets/Data/SAR_processing/radar_data_products/Sentinel-1/'
stack_name = 'Iceland_T155_descending_VV_test_camp'
path=tiff_folder + stack_name + '/' + 'calibrated_amplitude/'
proj = os.listdir(path)[0]
pol = 'VV'
# make folder for nc
# os.makedirs(tiff_folder + stack_name + '/camp_nc')
camp_path = tiff_folder + stack_name + '/' + 'calibrated_amplitude/' + proj + '/'
os.chdir(camp_path)
camp = 'db_' + pol + '@' + proj + '_in_coor_radar.tiff'
camp_filenames = glob.glob('*' + camp)
print(camp_filenames[1])
camp_new = gdal.Open(camp_path + camp_filenames[1]).ReadAsArray()