import os
import glob
import imageio
import numpy as np
import xarray as xr
from osgeo import gdal
import drama.utils as drtls
import matplotlib.pyplot as plt
import matplotlib
from cartopy import config
import cartopy
import cartopy.crs as ccrs
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker

class Vis_img_ani(object):
    
    def __init__(self, tpt_folder, tpt_stack_name, tiff_folder, stack_name, pol, rml):

        self.tpt_folder = tpt_folder
        self.tpt_stack_name = tpt_stack_name
        self.tiff_folder = tiff_folder
        self.stack_name = stack_name

        path = tiff_folder + stack_name + '/' + 'interferogram/'
        self.proj = os.listdir(path)[0]
        self.res = int(self.proj.split('0_')[1])

        self.pol = pol
        self.rml = rml

        self.date_pairs = self.list_of_date_pair()
        
        # load geodata
        
        self.lat = self.load_ncdata(os.path.join(self.tpt_folder, 'geocode', 'data', 'lat.nc'))
        self.lon = self.load_ncdata(os.path.join(self.tpt_folder, 'geocode', 'data', 'lon.nc'))
        self.mask_data = mask_data
        
    def load_ncdata(self, path):
        ncdata = xr.open_dataarray(path).data
        return ncdata
        
    def plot_tpt(self):
        fig = plt.figure(figsize=(12, 9))
        img_extent = [lon.min(), lon.max(), lat.min(), lat.max()]
        ax = plt.axes(projection=ccrs.Mercator())
        ax.set_extent(img_extent, ccrs.PlateCarree())
        draw_labels = True
        gl = ax.gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
        xlabels_top=False
        xlabels_bottom=True
        ylabels_left=False
        ylabels_right=True
        if draw_labels:
            gl.top_labels = xlabels_top
            gl.left_labels = ylabels_left
            gl.bottom_labels = xlabels_bottom
            gl.right_labels = ylabels_right
            gl.xlines = True
            gl.ylines = True
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            gl.xlabel_style = {'size': 12}
            gl.ylabel_style = {'size': 12}
        
        sea_10m = cartopy.feature.NaturalEarthFeature('physical', 'ocean', '10m',
                                                edgecolor='face',
                                                facecolor=cartopy.feature.COLORS['water'])
        ax.add_feature(sea_10m, zorder=1)
        im = ax.pcolormesh(lon, lat, np.degrees(tpt0_aml[:-1,:]), transform=ccrs.PlateCarree(), zorder=1, vmin=-60,vmax=60,cmap='bwr')
        cbar_ax = fig.add_axes([0.95, 0.13, 0.02, 0.75])
        clb = fig.colorbar(im, cax=cbar_ax)
        clb.set_label('$\phi_{ijk}$ [deg]', fontsize=15, labelpad=-32, y=1.07, rotation=0)
        ax.set_ylabel('Latitude [deg]', fontsize=20)
        ax.set_xlabel('Longitude [deg]', fontsize=20)
        plt.savefig(os.path.join(self.tpt_folder, 'triplets', 'img', dates + '_tpt.png'),dpi=300)
        plt.close()

#     def plot_amp(self, dates):
#         '''
#         plot re-multilooked data
#         :param dates:
#         :return:
#         '''
        
#         three_dates = dates.split('_')
        
#         # load amplitude data
#         amp1 = self.load_ncdata(os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[0] + '_amp.nc'))
#         amp2 = self.load_ncdata(os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[1] + '_amp.nc'))
#         amp3 = self.load_ncdata(os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[2] + '_amp.nc'))
        
#         #  plot amplitude
#         fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
#         im1 = ax[0].pcolormesh(self.lon, self.lat, 10 * np.log10(amp1), vmax=0, vmin=-30, cmap='bone')
#         im2 = ax[1].pcolormesh(self.lon, self.lat, 10 * np.log10(amp2), vmax=0, vmin=-30, cmap='bone')
#         im3 = ax[2].pcolormesh(self.lon, self.lat, 10 * np.log10(amp3), vmax=0, vmin=-30, cmap='bone')
#         ax[0].grid()
#         ax[0].set_title(dates[0], fontsize=20, y=1.02)
#         ax[0].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[0].set_xlabel('Longitude [deg]', fontsize=20)
#         ax[1].grid()
#         ax[1].set_title(dates[1], fontsize=20, y=1.02)
#         ax[1].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[1].set_xlabel('Longitude [deg]', fontsize=20)
#         ax[2].grid()
#         ax[2].set_title(dates[2], fontsize=20, y=1.02)
#         ax[2].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[2].set_xlabel('Longitude [deg]', fontsize=20)
#         fig.subplots_adjust(right=0.85)
#         cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
#         clb = fig.colorbar(im3, cax=cbar_ax)
#         clb.set_label('$\sigma_0$ [dB]', fontsize=15, labelpad=-32, y=1.07, rotation=0)
#         plt.savefig(os.path.join(self.tpt_folder, 'amplitude', 'img', dates + '_amp.png'), dpi=300)
#         plt.close()
        
#     def plot_coh(self, dates):
#         '''
#         plot re-multilooked data
#         :param dates:
#         :return:
#         '''
        
#         three_dates = dates.split('_')
#         # load coherence data

#         coh12 = self.load_ncdata(os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[1] + '_coh.nc'))
#         coh23 = self.load_ncdata(coh23, os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[1] + '_' + three_dates[2] + '_coh.nc'))
#         coh13 = self.load_ncdata(coh13, os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[2] + '_coh.nc'))
        
#         # mask coherence
#         coh12 = np.ma.masked_array(coh12, np.logical_not(self.mask_data != 0))
#         coh23 = np.ma.masked_array(coh23, np.logical_not(self.mask_data != 0))
#         coh13 = np.ma.masked_array(coh13, np.logical_not(self.mask_data != 0))

#         #  plot coherence
#         fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(23, 6), dpi=300)
#         im1 = ax[0].pcolormesh(self.lon, self.lat, coh12, vmax=1, vmin=0, cmap='bone')
#         im2 = ax[1].pcolormesh(self.lon, self.lat, coh23, vmax=1, vmin=0, cmap='bone')
#         im3 = ax[2].pcolormesh(self.lon, self.lat, coh13, vmax=1, vmin=0, cmap='bone')
#         ax[0].grid()
#         ax[0].set_title(dates[0] + '_' + dates[1], fontsize=20, y=1.02)
#         ax[0].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[0].set_xlabel('Longitude [deg]', fontsize=20)
#         ax[1].grid()
#         ax[1].set_title(dates[1] + '_' + dates[2], fontsize=20, y=1.02)
#         ax[1].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[1].set_xlabel('Longitude [deg]', fontsize=20)
#         ax[2].grid()
#         ax[2].set_title(dates[0] + '_' + dates[2], fontsize=20, y=1.02)
#         ax[2].set_ylabel('Latitude [deg]', fontsize=20)
#         ax[2].set_xlabel('Longitude [deg]', fontsize=20)
#         fig.subplots_adjust(right=0.85)
#         cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.69])
#         clb = fig.colorbar(im3, cax=cbar_ax)
#         clb.set_label('$\gamma$', fontsize=15, labelpad=-32, y=1.07, rotation=0)
#         plt.savefig(os.path.join(self.tpt_folder, 'coherence', 'img', dates + '_coh.png'), dpi=300)
#         plt.close()
        
#     def plot_tpt(self, dates):
        
#         tpt = self.load_ncdata(os.path.join(self.tpt_folder, 'triplets', 'data', dates + '_tpt.nc'))

#         # mask data
#         tpt_mask = np.ma.masked_array(tpt, np.logical_not(self.mask_data != 0))

#         #  plot tpt phase
#         plt.figure(figsize=(12, 9), dpi=300)
#         plt.pcolormesh(self.lon, self.lat, np.degrees(tpt_mask), cmap='bwr', vmin=-60, vmax=60)
#         clb = plt.colorbar()
#         clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
#         plt.grid()
#         plt.ylabel('Latitude [deg]', fontsize=20)
#         plt.xlabel('Longitude [deg]', fontsize=20)
#         # plt.title(dates, fontsize=20, y=1.02)
#         plt.savefig(os.path.join(self.tpt_folder, 'triplets', 'img', dates + '_tpt.png'),dpi=300)
#         plt.close()

#     def animate_from_images(self):
#         '''
#         animation from images
#         :return:
#         '''

#         if self.rml != 2:
#             res = int(self.res * self.rml / 2)  # resolution
#             image_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/img/'
#             gif_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/amplitude/ani/' + str(res) + '_amp.gif'
#             image_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/img/'
#             gif_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/ani/' + str(res) + '_phase.gif'
#             image_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/img/'
#             gif_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/coherence/ani/' + str(res) + '_coh.gif'
#         else:
#             res = self.res
#             image_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/amplitude/img/'
#             gif_path_amp = self.tpt_folder + '/' + self.tpt_stack_name + '/amplitude/ani/' + str(res) + '_amp.gif'
#             image_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/img/'
#             gif_path_phase = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/ani/' + str(res) + '_phase.gif'
#             image_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/coherence/img/'
#             gif_path_coh = self.tpt_folder + '/' + self.tpt_stack_name + '/coherence/ani/' + str(res) + '_coh.gif'

#         # animate amplitude
#         os.chdir(image_path_amp)

#         filenames = glob.glob('*' + str(res) + '_amp.png')
#         image_list = []
#         for filename in filenames:
#             image_list.append(imageio.imread(image_path_amp + filename))
#         imageio.mimwrite(gif_path_amp, image_list, format='GIF', duration=0.5)

#         # animate phase
#         os.chdir(image_path_phase)

#         filenames = glob.glob('*' + str(res) + '_phase.png')
#         image_list = []
#         for filename in filenames:
#             image_list.append(imageio.imread(image_path_phase + filename))
#         imageio.mimwrite(gif_path_phase, image_list, format='GIF', duration=0.5)

#         # animate coherence
#         os.chdir(image_path_coh)

#         filenames = glob.glob('*' + str(res) + '_coh.png')
#         image_list = []
#         for filename in filenames:
#             image_list.append(imageio.imread(image_path_coh + filename))
#         imageio.mimwrite(gif_path_coh, image_list, format='GIF', duration=0.5)
