class Boxcar_ML(object):
    def __init__(self, tiff_folder, tpt_folder, stack_name, win_size):
        self.proj = proj
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)

        self.tiff_folder = os.path.join(tiff_folder,stack_name)
        self.tpt_folder = os.path.join(tpt_folder,stack_name, self.ml_proj + '_BC')
        self.tpt_dates = self.list_of_tptdates()
                
    def list_of_tptdates(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        path = os.path.join(self.tiff_folder, 'interferogram', self.proj)
        ifg_dates = os.listdir(path)
        dates = []
        for ii in ifg_dates:
            dates.append(ii.split('_int')[0])
        # creat dates triplets 
        ind = np.linspace(0, len(dates) - 3, int((len(dates) - 1) / 2))
        tpt_dates = []
        for ii in ind:
            ii = int(ii)
            tpt_dates.append(dates[ii] + '_'+ dates[ii+2].split('_')[1])
        return tpt_dates
    
    def multilooking(self, tptdates):
        dates = tptdates.split('_')
        
        # read in amplitude to construct phasor
        nc_path = os.path.join(self.tiff_folder, 'camp_nc')
        os.chdir(nc_path)
        filename = glob.glob(dates[0] + '*')
        amp1 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        filename = glob.glob(dates[1] + '*')
        amp2 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        filename = glob.glob(dates[2] + '*')
        amp3 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        
        # read in coherence data
        path = os.path.join(self.tiff_folder, 'coherence', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        coh12 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        coh23 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        coh13 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()
        
        # read in inteferometric phase
        path = os.path.join(self.tiff_folder, 'interferogram/', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        phi12 = gdal.Open(os.path.join(path, filename[0]))
        phi12 = phi12.GetRasterBand(2).ReadAsArray()
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        phi23 = gdal.Open(os.path.join(path, filename[0]))
        phi23 = phi23.GetRasterBand(2).ReadAsArray()
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        phi13 = gdal.Open(os.path.join(path, filename[0]))
        phi13 = phi13.GetRasterBand(2).ReadAsArray()
        
        # construct phasor
        coh12 = coh12 * np.exp(1j * phi12) * amp1 * amp2
        coh23 = coh23 * np.exp(1j * phi23) * amp2 * amp3
        coh13 = coh13 * np.exp(1j * phi13) * amp1 * amp3
        
        # multilooking
        ml_amp1 = np.sqrt(drtls.smooth(np.abs(amp1)**2,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::]
                          [::self.win_size,::self.win_size])
        ml_amp2 = np.sqrt(drtls.smooth(np.abs(amp2)**2,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::]
                          [::self.win_size,::self.win_size])
        ml_amp3 = np.sqrt(drtls.smooth(np.abs(amp3)**2,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::]
                          [::self.win_size,::self.win_size])

        ml_coh12 = drtls.smooth(coh12,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
        ml_coh23 = drtls.smooth(coh23,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
        ml_coh13 = drtls.smooth(coh13,self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
        
        # scale 
        ml_coh12 = ml_coh12/(ml_amp1*ml_amp2)
        ml_coh23 = ml_coh23/(ml_amp2*ml_amp3)
        ml_coh13 = ml_coh13/(ml_amp1*ml_amp3)
        
        return ml_amp1, ml_amp2, ml_amp3, ml_coh12, ml_coh23, ml_coh13
    
    def save_data(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)
    
    def save_ml_data(self):

        for dates in self.tpt_dates:
            amp1, amp2, amp3, coh12, coh23, coh13 = self.multilooking(dates)
            three_dates = dates.split('_')

            # save amplitude data
            self.save_data(amp1, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[0] + '_amp.nc'))
            self.save_data(amp2, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[1] + '_amp.nc'))
            self.save_data(amp3, os.path.join(self.tpt_folder, 'amplitude', 'data', three_dates[2] + '_amp.nc'))

            # save coherence data
            self.save_data(np.abs(coh12), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[1] + '_coh.nc'))
            self.save_data(np.abs(coh23), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[1] + '_' + three_dates[2] + '_coh.nc'))
            self.save_data(np.abs(coh13), os.path.join(self.tpt_folder, 'coherence', 'data', three_dates[0] + '_' + three_dates[2] + '_coh.nc'))

            # save triplets data
            ifg12 = coh12*amp1*amp2
            ifg23 = coh23*amp2*amp3
            ifg13 = coh13*amp1*amp3
            tpt = np.angle(ifg12 * ifg23 * np.conj(ifg13))
            self.save_data(tpt, os.path.join(self.tpt_folder, 'triplets', 'data', dates + '_tpt.nc'))
            print(dates)