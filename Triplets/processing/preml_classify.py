import os
import glob
import numpy as np
import xarray as xr
from osgeo import gdal
import scipy.stats as stats

class Amp_classify(object):
    def __init__(self, stack_name, thred, win_size, nimg, dimg, mask_data, datastack):
        self.threds = stats.foldnorm.ppf(thred, 0)# generally set to 1-5%
        self.win_size = win_size
        self.nimg = nimg # length of time series
        self.dimg = dimg # shape of each image
        self.mask_data = mask_data
        self.stack_name = stack_name
        self.datastack = datastack
    
    def mean_test(self, ref_arr, arr):
        mean_ref = np.mean(ref_arr)
        std_ref = np.std(ref_arr)
        tmean = (np.mean(arr)-mean_ref)*np.sqrt(self.nimg)/std_ref
        if np.abs(tmean) <= self.threds:
            return True
    
    def sub_classify(self, ind_ii, ind_jj, sub_class):
        class_id = int(1)
        while np.min(sub_class)==0:
            ind_row, ind_col = np.where(sub_class == 0)
            sub_class[ind_row[0], ind_col[0]] = class_id
            arr_ref = self.datastack[:,int(ind_ii + ind_row[0]),int(ind_jj + ind_col[0])].data
            for ii in np.arange(ind_row.shape[0]-1):
                arr_com = self.datastack[:,int(ind_ii + ind_row[ii+1]),int(ind_jj + ind_col[ii+1])].data
                if self.mean_test(arr_ref,arr_com):
                    sub_class[ind_row[ii+1], ind_col[ii+1]] = class_id 
            class_id = int(class_id + 1)
        return sub_class

    def tot_classify(self):
        ndim = np.ceil(self.dimg/self.win_size)
        class_map = np.zeros(self.dimg)
        class_map = np.where(self.mask_data!=0, class_map, 9999)
        # itsdone = 0
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                # t1 = time.time()
                sub_map = class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_clas_map = self.sub_classify(ind_ii, ind_jj, sub_map)
                class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)] = sub_clas_map
                # dt = time.time()-t1
            print(ii)
        return class_map
    
    def save_map(self, data, path):
        data = xr.DataArray(data)
        data.to_netcdf(path)