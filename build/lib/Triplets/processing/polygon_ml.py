import os
import glob
import shapely
import numpy as np
import xarray as xr
import pandas as pd
from osgeo import gdal
import geopandas as gpd
import warnings
warnings.filterwarnings("ignore")

class Poly_ML(object):
    
    def __init__(self, tiff_folder, out_folder, stack_name, pol, proj, read_region, pointInPolys, field_shp):
        self.pol = pol
        self.proj = proj
        self.read_region = read_region
        self.out_folder = out_folder
        self.tiff_folder = os.path.join(tiff_folder,stack_name)
        self.tpt_dates = self.list_of_tptdates()
        self.pointInPolys = pointInPolys
        self.field_shp = field_shp # shapefiles for target fields
                
    def list_of_tptdates(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        path = os.path.join(self.tiff_folder, 'calibrated_amplitude', self.proj)
        os.chdir(path)
        camp = 'db_' + self.pol + '@' + self.proj + '_in_coor_radar.tiff'
        camp_filenames = glob.glob('*' + camp)
        dates = []
        for ii in camp_filenames:
            dates.append(ii.split('_cal')[0])
        # creat dates triplets 
        dates = sorted(dates)
        ind = np.arange(len(dates)-2)
        tpt_dates = []
        for ii in ind:
            day12 = (pd.to_datetime(dates[ii+1]).date()-pd.to_datetime(dates[ii]).date()).days
            day23 = (pd.to_datetime(dates[ii+2]).date()-pd.to_datetime(dates[ii+1]).date()).days
            if np.logical_and(day12==6, day23==6):
                tpt_dates.append(dates[ii] + '_'+ dates[ii+1]+ '_'+ dates[ii+2])
        return tpt_dates
    
    def new_shp(self):
        '''
        add columns to shapefile
        '''
        new_field_shp = self.field_shp.assign(Cal_amp1 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Cal_amp2 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Cal_amp3 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Ifg12 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Ifg23 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Ifg13 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Coherence12 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Coherence23 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Coherence13 = np.NaN * np.arange(gh_shp.shape[0]))
        new_field_shp = new_field_shp.assign(Tri_phi = np.NaN * np.arange(gh_shp.shape[0]))
        
        columns_to_delete = ['CD_PROV','CD_MUN','CD_POL','CD_PARCELA', 'CD_RECINTO', 'CD_USO','NU_AREA','PC_PASTOS','COEF_REG','PDTE_MEDIA','INCIDENCIA','REGION','GC']
        new_field_shp.drop(columns = columns_to_delete)
        
        return new_field_shp

    
    def load_2d_data(self, tptdates):
        dates = tptdates.split('_')
        
        # read in amplitude to construct phasor
        nc_path = os.path.join(self.tiff_folder, 'camp_nc')
        os.chdir(nc_path)
        filename = glob.glob(dates[0] + '*')
        amp1 = xr.open_dataarray(os.path.join(nc_path, filename[0])).data
        amp1 = gdal.Open(os.path.join(nc_path, filename[0])).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[1] + '*')
        amp2 = gdal.Open(os.path.join(nc_path, filename[0])).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[2] + '*')
        amp3 = gdal.Open(os.path.join(nc_path, filename[0])).ReadAsArray(*self.read_region)

        
        # read in coherence data
        path = os.path.join(self.tiff_folder, 'coherence', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        coh12 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        coh23 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        coh13 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray(*self.read_region)

        
        # read in inteferometric phase
        path = os.path.join(self.tiff_folder, 'interferogram/', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '_' + dates[1] + '*')
        phi12 = gdal.Open(os.path.join(path, filename[0]))
        phi12 = phi12.GetRasterBand(2).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[1] + '_' + dates[2] + '*')
        phi23 = gdal.Open(os.path.join(path, filename[0]))
        phi23 = phi23.GetRasterBand(2).ReadAsArray(*self.read_region)
        filename = glob.glob(dates[0] + '_' + dates[2] + '*')
        phi13 = gdal.Open(os.path.join(path, filename[0]))
        phi13 = phi13.GetRasterBand(2).ReadAsArray(*self.read_region)
        
        
        # construct phasor
        coh12 = coh12 * np.exp(1j * phi12) * amp1 * amp2
        coh23 = coh23 * np.exp(1j * phi23) * amp2 * amp3
        coh13 = coh13 * np.exp(1j * phi13) * amp1 * amp3
        
        return amp1, amp2, amp3, coh12, coh23, coh13
    
    def multilooking(self, tptdates):
        
        amp1, amp2, amp3, coh12, coh23, coh13 = self.load_2d_data(tptdates)
        
        # multilook for each polygon and add as attributes to shapefiles
        
        self.new_field_shp = self.new_shp()
        
        poly_id = self.new_field_shp.ID_RECINTO.values

        for poly in poly_id:
            ind_polygon_id = np.where(self.new_field_shp.ID_RECINTO==poly)[0]
            ind_point_id = np.where(self.pointInPolys.ID_RECINTO==poly)[0]
            point_id = self.pointInPolys.iloc[ind_point_id].index_left.values
            if np.any(~np.isnan(point_id)):
                # multilook amplitudes
                ml_amp1 = np.sqrt(np.mean(amp1.flatten()[point_id.astype(int)]**2))
                ml_amp2 = np.sqrt(np.mean(amp2.flatten()[point_id.astype(int)]**2))
                ml_amp3 = np.sqrt(np.mean(amp3.flatten()[point_id.astype(int)]**2))
                # multilook coherence
                ml_coh12 = np.mean(coh12.flatten()[point_id.astype(int)])/(ml_amp1*ml_amp2)
                ml_coh23 = np.mean(coh23.flatten()[point_id.astype(int)])/(ml_amp2*ml_amp3)
                ml_coh13 = np.mean(coh13.flatten()[point_id.astype(int)])/(ml_amp1*ml_amp3)
                # multilook interferogram
                ml_ifg12 = ml_coh12*ml_amp1*ml_amp2
                ml_ifg23 = ml_coh23*ml_amp2*ml_amp3
                ml_ifg13 = ml_coh13*ml_amp1*ml_amp3
                # multilook closure phase
                ml_tpt = np.angle(ml_ifg12 * ml_ifg23 * np.conj(ml_ifg13))
            

            self.new_field_shp.Cal_amp1[ind_polygon_id] = ml_amp1
            self.new_field_shp.Cal_amp2[ind_polygon_id] = ml_amp2
            self.new_field_shp.Cal_amp3[ind_polygon_id] = ml_amp3
            
            self.new_field_shp.Ifg12[ind_polygon_id] = np.angle(ml_ifg12)
            self.new_field_shp.Ifg23[ind_polygon_id] = np.angle(ml_ifg23)
            self.new_field_shp.Ifg13[ind_polygon_id] = np.angle(ml_ifg13)
            self.new_field_shp.Coherence12[ind_polygon_id] = np.abs(ml_coh12)
            self.new_field_shp.Coherence23[ind_polygon_id] = np.abs(ml_coh23)
            self.new_field_shp.Coherence13[ind_polygon_id] = np.abs(ml_coh13)
            self.new_field_shp.Tri_phi[ind_polygon_id] = ml_tpt
            
    def save_new_shp(self):
        
        for dates in self.tpt_dates:
            self.multilooking(tptdate)
            self.new_field_shp.to_file(os.path.join(self.out_folder, dates + '_ml.shp'))
            print(dates)