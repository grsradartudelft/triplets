import os
import glob
import numpy as np
import xarray as xr
from osgeo import gdal
import scipy.stats as stats
import drama.utils as drtls

class ML_htp(object):
    '''
    generating one-way baseline
    in order to get the 2-way vertical baseline (kappa) triplets
    needs to divide the end products by 8
    '''
    def __init__(self, tiff_folder, tpt_folder, stack_name, win_size, pol, proj, slice_index):
        self.pol = pol
        self.proj = proj
        self.wvl = 5.5465763e-2
        self.win_size = win_size
        self.ml_proj = str(win_size * 20)
        self.slice = slice_index
        self.tiff_folder = os.path.join(tiff_folder, stack_name)
        self.tpt_folder = os.path.join(tpt_folder, stack_name, self.ml_proj)
        self.tpt_dates = self.load_tptdates()
        
                    
    def load_tptdates(self):
        '''
        list of date pairs of interferogram
        :return:
        '''
        filenames = os.listdir(os.path.join(self.tpt_folder,'triplets', 'data'))
        tpt_dates = []
        for filename in filenames:
            tpt_dates.append(filename[:-7])
        return tpt_dates
        
    def load_htp(self, tptdates):
        '''
        load data
        :param dates:
        :param data_type:
        :return:
        '''
        dates = tptdates.split('_')
        path = os.path.join(self.tiff_folder, 'height_to_phase', self.proj)
        os.chdir(path)
        filename = glob.glob(dates[0] + '*')
        k1 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[1] + '*')
        k2 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        filename = glob.glob(dates[2] + '*')
        k3 = gdal.Open(os.path.join(path, filename[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        return k1, k2, k3
    
    def intf_htp(self, tri_date):
        k1, k2, k3 = self.load_htp(tri_date)
        k12 = k1 - k2
        k23 = k2 - k3
        k31 = k3 - k1
        return k12, k23, k31
                
    
    def tpt_htp(self, tri_date):
        k12, k23, k31 = self.intf_htp(tri_date)
        k123 = (8*np.pi/self.wvl)**3 * k12 * k23 * k31
        self.dimg = np.array([k123.shape[0], k123.shape[1]])
        return k123
                
    def ml_htp(self):
        '''
        re-multilooking data
        :param data_type:
        :return:
        '''
        for tri_date in self.tpt_dates:
            print(tri_date)
            k123 = self.tpt_htp(tri_date)
            htp = drtls.smooth(k123, self.win_size,axis=None)[int(np.floor(self.win_size/2))::,int(np.floor(self.win_size/2))::][::self.win_size,::self.win_size]
            self.save_data(htp, os.path.join(self.tpt_folder, 'height_to_phase', 'data', tri_date + '_htp.nc'))
            
    def cor_shape(self, dataset):
        dim = np.floor(self.dimg/self.win_size)
        if dataset.shape[0]>dim[0]:
            dataset = dataset[:-1,:]
        if dataset.shape[1]>dim[1]:
            dataset = dataset[:,:-1]
        return dataset
    
    def save_data(self, data, path):
        data = self.cor_shape(data)
        data = xr.DataArray(data)
        data.to_netcdf(path)
