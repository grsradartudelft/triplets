import os
import numpy as np
import xarray as xr
from osgeo import gdal
import scipy.stats as stats
from matplotlib import colors
from matplotlib import ticker

class Landcover_match(object):
    def __init__(self, tiff_folder, tpt_folder, stack_name, win_size, sar_area, slice_index):
        self.tiff_folder = tiff_folder
        self.tpt_folder = tpt_folder
        self.stack_name = stack_name
        self.win_size = win_size
        self.map_path = '/tudelft.net/staff-umbrella/Triplets/Data/Application/Land_cover_map/2017/'
        self.map_res = 1/1008
        self.sar_area = sar_area
        # sar_area = np.array([lon_w, lon_e, lat_s, lat_n]) each is integer, make it slightly wider but do not exceed the edge of map
        self.lat_edge = False
        self.lon_edge = False
        self.area = self.select_area()
        self.Maps = self.land_map()
        self.class_map = xr.open_dataarray(os.path.join(self.tpt_folder,self.stack_name, str(self.win_size*20), 'classify_map', 'data','map.nc')).data
        self.dimg = np.array([self.class_map.shape[0], self.class_map.shape[1]])
        self.slice = slice_index
        
    def edge_check(self):
        if self.sar_area[0] < 0-self.map_res and self.sar_area[1] > 0-self.map_res:
            self.lon_edge = True
        if self.sar_area[2] < 40-self.map_res and self.sar_area[3] > 40-self.map_res:
            self.lat_edge = True
            
    def select_area(self):
        self.edge_check()
        if self.lat_edge and self.lon_edge == False:
            return 'W020N40', 'W020N60'
        elif self.lon_edge and self.lat_edge == False:
            return 'W020N60', 'E000N60'
        elif self.lat_edge==False and self.lon_edge == False:
            if self.sar_area[0] <= 0-self.map_res and self.sar_area[3]<=40-self.map_res:
                return 'W020N40'
            elif self.sar_area[0] <= 0-self.map_res and self.sar_area[2]>=40-self.map_res:
                return 'W020N60'
            elif self.sar_area[0] >= 0-self.map_res and self.sar_area[2]>=40-self.map_res:
                return 'E000N60'
            
    def slice_map(self, map_area, data_area):
        x_low, x_up, y_low, y_up = self.indxy(map_area)
        x = np.linspace(x_low, x_up, 20160)
        y = np.linspace(y_low, y_up, 20160)
        slice0=np.where((y-data_area[3])==np.sign(y-data_area[3])*np.min(np.abs(y-data_area[3])))[0][0]
        slice1=np.where((y-data_area[2])==np.sign(y-data_area[2])*np.min(np.abs(y-data_area[2])))[0][0]
        slice2=np.where((x-data_area[0])==np.sign(x-data_area[0])*np.min(np.abs(x-data_area[0])))[0][0]
        slice3=np.where((x-data_area[1])==np.sign(x-data_area[1])*np.min(np.abs(x-data_area[1])))[0][0]
        return slice0, slice1, slice2, slice3
            
    def land_map(self):
        if len(self.area)==7:
            slice0, slice1, slice2, slice3 = self.slice_map(self.area, self.sar_area)
            return gdal.Open(os.path.join(self.map_path, self.area, 'original.tif')).ReadAsArray()[slice0:int(slice1+1),slice2:int(slice3+1)]
        elif len(self.area)==2:
            if self.lat_edge:
                data_area = np.array([self.sar_area[0], self.sar_area[1], self.sar_area[2], 40])
                slice00, slice10, slice20, slice30 = self.slice_map(self.area[0], data_area)
                map1 = gdal.Open(os.path.join(self.map_path, self.area[0], 'original.tif')).ReadAsArray()[slice00:int(slice10+1),slice20:int(slice30+1)]
                data_area = np.array([self.sar_area[0], self.sar_area[1], 40-self.map_res, self.sar_area[3]])
                slice01, slice11, slice21, slice31 = self.slice_map(self.area[1], data_area)
                map2 = gdal.Open(os.path.join(self.map_path, self.area[1], 'original.tif')).ReadAsArray()[slice01:int(slice11+1),slice21:int(slice31+1)]
                ad_map = np.vstack((map2, map1))
                return ad_map
            elif self.lon_edge:
                data_area = np.array([self.sar_area[0], 0-self.map_res, self.sar_area[2], self.sar_area[3]])
                slice00, slice10, slice20, slice30 = self.slice_map(self.area[0], data_area)
                map1 = gdal.Open(os.path.join(self.map_path, self.area[0], 'original.tif')).ReadAsArray()[slice00:int(slice10+1),slice20:int(slice30+1)]
                data_area = np.array([0, self.sar_area[1], self.sar_area[2], self.sar_area[3]])
                slice01, slice11, slice21, slice31 = self.slice_map(self.area[1], data_area)
                map2 = gdal.Open(os.path.join(self.map_path, self.area[1], 'original.tif')).ReadAsArray()[slice01:int(slice11+1),slice21:int(slice31+1)]
                ad_map = np.hstack((map1, map2))
                return ad_map         
            
    def indxy(self, map_area):
        if map_area[0] == 'W':
            x_low = -int(map_area[1:4])
            x_up = x_low + 20-self.map_res
        else:
            x_low = int(map_area[1:4])
            x_up = x_low + 20-self.map_res
        if map_area[4] == 'N':
            y_low = int(map_area[5:7])
            y_up = y_low - 20+self.map_res
        else:
            y_low = -int(map_area[5:7])
            y_up = y_low - 20+self.map_res
        return x_low, x_up, y_low, y_up

    def corner(self):
        if len(self.area)==7:
            x_low, x_up, y_low, y_up = self.indxy(self.area)
            x = np.linspace(x_low, x_up, 20160)
            y = np.linspace(y_low, y_up, 20160)
            slice0, slice1, slice2, slice3 = self.slice_map(self.area, self.sar_area) 
            lat_n = y[slice0]
            lat_s = y[slice1]
            lon_w = x[slice2]
            lon_e = x[slice3]
            return  lon_w, lon_e, lat_n, lat_s 
        elif len(self.area)==2:
            if self.lat_edge:
                data_area = np.array([self.sar_area[0], self.sar_area[1], self.sar_area[2], 40])
                x_low, x_up, y_low, y_up = self.indxy(self.area[0])
                x = np.linspace(x_low, x_up, 20160)
                y = np.linspace(y_low, y_up, 20160)
                slice00, slice10, slice20, slice30 = self.slice_map(self.area[0], data_area)
                lon_w0 = x[slice20]
                lon_e0 = x[slice30] 
                lat_n0 = y[slice00]
                lat_s0 = y[slice10]
                
                data_area = np.array([self.sar_area[0], self.sar_area[1], 40-self.map_res, self.sar_area[3]])
                x_low, x_up, y_low, y_up = self.indxy(self.area[1])
                x = np.linspace(x_low, x_up, 20160)
                y = np.linspace(y_low, y_up, 20160)
                slice01, slice11, slice21, slice31 = self.slice_map(self.area[1], data_area)
                lon_w1 = x[slice21]
                lon_e1 = x[slice31] 
                lat_n1 = y[slice01]
                lat_s1 = y[slice11]
                return lon_w0, lon_e0, lat_n1, lat_s0
            
            elif self.lon_edge:
                data_area = np.array([self.sar_area[0], 0-self.map_res, self.sar_area[2], self.sar_area[3]])
                x_low, x_up, y_low, y_up = self.indxy(self.area[0])
                x = np.linspace(x_low, x_up, 20160)
                y = np.linspace(y_low, y_up, 20160)
                slice00, slice10, slice20, slice30 = self.slice_map(self.area[0], data_area)
                lon_w0 = x[slice20]
                lon_e0 = x[slice30] 
                lat_n0 = y[slice00]
                lat_s0 = y[slice10]
                
                data_area = np.array([0, self.sar_area[1], self.sar_area[2], self.sar_area[3]])
                x_low, x_up, y_low, y_up = self.indxy(self.area[1])
                x = np.linspace(x_low, x_up, 20160)
                y = np.linspace(y_low, y_up, 20160)
                slice01, slice11, slice21, slice31 = self.slice_map(self.area[1], data_area)
                lon_w1 = x[slice21]
                lon_e1 = x[slice31] 
                lat_n1 = y[slice01]
                lat_s1 = y[slice11]
            return  lon_w0, lon_e1, lat_n0, lat_s0           
    
    def main_class(self):
        ndim = np.ceil(self.dimg/self.win_size)
        main_map = np.zeros(self.dimg)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_map = np.where(sub_map == stats.mode(sub_map,axis=None)[0][0], 1, 0)
                main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)] = sub_map
        return main_map
    
    def geocode_sar(self):
        path=os.path.join(self.tiff_folder, self.stack_name, 'calibrated_amplitude')
        proj = os.listdir(path)[0]
        path = os.path.join(self.tiff_folder, self.stack_name, 'geocode', proj)
        paths = os.listdir(path)
        # lat_sar = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()
        # lon_sar = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()
        lat_sar = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        lon_sar = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()[self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        x_low, x_up, y_low, y_up = self.corner()
        dlat = (y_up - y_low)/(self.Maps.shape[0]-1)
        dlon = (x_up - x_low)/(self.Maps.shape[1]-1)
        lat_ind = np.round((lat_sar - y_up) / dlat).astype(int)
        lon_ind = np.round((lon_sar - x_low) / dlon).astype(int)
        # Maps_sar = self.Maps[lat_ind, lon_ind][self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        Maps_sar = self.Maps[lat_ind, lon_ind]
        return Maps_sar
    
    def land_sar(self):
        main_map = self.main_class()
        ndim = np.ceil(self.dimg/self.win_size).astype(int)
        # mask Maps_sar
        Maps_sar = self.geocode_sar()
        Maps_sar = np.where(main_map!=0, Maps_sar, np.NaN)
        land_id = np.zeros(ndim)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_Maps_sar = Maps_sar[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                land_id[ii, jj] = stats.mode(sub_Maps_sar,axis=None)[0][0]
        land_id = self.cor_shape(land_id)
        return land_id
    
    def cor_shape(self, dataset):
        dim = np.floor(self.dimg/self.win_size)
        if dataset.shape[0]>dim[0]:
            dataset = dataset[:-1,:]
        if dataset.shape[1]>dim[1]:
            dataset = dataset[:,:-1]
        return dataset
    
    def plt_dicts(self):
        id_array = np.array([20,  30,  40,  50,  60,  70, 80,  90, 100, 111, 112, 113, 114, 115, 116, 121, 122, 123, 124, 125, 126, 200])
        col_dict = dict(zip(id_array, [(255/255, 187/255, 34/255), (255/255,255/255,76/255), (240/255, 150/255, 255/255), (250/255, 0 ,0), 
                       (180/255, 180/255, 180/255), (240/255, 240/255, 240/255), (0, 50/255, 200/255), (0, 150/255, 160/255), (250/255, 230/255, 160/255),
                       (88/255, 72/255, 31/255), (0, 153/255, 0), (112/255, 102/255, 62/255), (0, 204/255, 0), (78/255, 117/255, 31/255), (0, 120/255, 0), 
                       (102/255, 96/255, 0), (141/255, 180/255, 0), (141/255, 116/255, 0), (160/255, 220/255, 0), (146/255, 153/255, 0), (100/255, 140/255, 0), 
                       (0, 0, 128/255)]))
        label_dict = dict(zip(id_array, ['Shrubs', 'Herbaceous vegetation', 'Cultivated and managed vegetation/agriculture (cropland)', 'Urban / built up', 
                           'Bare / sparse vegetation', 'Snow and ice', 'Permanent water bodies', 'Herbaceous wetland', 'Moss and lichen',
                           'Closed forest, evergreen needle leaf', 'Closed forest, evergreen, broad leaf', 'Closed forest, deciduous needle leaf', 
                           'Closed forest, deciduous broad leaf', 'Closed forest, mixed', 'Closed forest, unknown', 'Open forest, evergreen needle leaf', 
                           'Open forest, evergreen broad leaf', 'Open forest, deciduous needle leaf', 'Open forest, deciduous broad leaf', 
                           'Open forest,mixed', 'Open forest, unknown', 'Open sea']))
        return col_dict, label_dict