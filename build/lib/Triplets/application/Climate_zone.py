import os
import numpy as np
import xarray as xr
from osgeo import gdal
import scipy.stats as stats
from matplotlib import colors
from matplotlib import ticker

class Climate_match(object):
    def __init__(self, tiff_folder, tpt_folder, stack_name, win_size, slice_index):
        self.y_low = -90
        self.y_up = 90
        self.x_low = -180
        self.x_up = 180
        self.tiff_folder = tiff_folder
        self.tpt_folder = tpt_folder
        self.stack_name = stack_name
        self.win_size = win_size
        self.climate_Maps = self.load_map()
        # self.climate_Maps = np.where(self.climate_Maps>0, self.climate_Maps, np.NaN)
        self.class_map = xr.open_dataarray(os.path.join(self.tpt_folder,self.stack_name, str(self.win_size*20), 'classify_map', 'data','map.nc')).data
        self.dimg = np.array([self.class_map.shape[0], self.class_map.shape[1]])
        self.slice = slice_index
        
    def load_map(self):
        maps_2007 = open(r'/tudelft.net/staff-umbrella/mwaves1triplets/Yan/Application/Climate_zones/koppen_2007.txt',"r")
        read_line = maps_2007.readlines()[6:]
        Map = np.zeros([3600, 1800])
        for ii in np.arange(len(read_line)):
            Map[:, ii] = np.asarray(list(map(int, np.asarray(read_line[ii].split()))))
        Map = Map.T
        return Map
    
    def main_class(self):
        ndim = np.ceil(self.dimg/self.win_size)
        main_map = np.zeros(self.dimg)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_map = self.class_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                sub_map = np.where(sub_map == stats.mode(sub_map,axis=None)[0][0], 1, 0)
                main_map[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)] = sub_map
        return main_map
    
    def geocode_sar(self):
        path=os.path.join(self.tiff_folder, self.stack_name, 'calibrated_amplitude')
        proj = os.listdir(path)[0]
        path = os.path.join(self.tiff_folder, self.stack_name, 'geocode', proj)
        paths = os.listdir(path)
        lat_sar = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()
        lon_sar = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()
        dlat = (self.y_low - self.y_up)/(self.climate_Maps.shape[0]-1)
        dlon = (self.x_up - self.x_low)/(self.climate_Maps.shape[1]-1)
        
        lat_ind = np.round((lat_sar - self.y_up) / dlat).astype(int)
        lon_ind = np.round((lon_sar - self.x_low) / dlon).astype(int)
        climate_Maps_sar = self.climate_Maps[lat_ind, lon_ind][self.slice[0]:self.slice[1], self.slice[2]:self.slice[3]]
        return climate_Maps_sar
    
    def climate_sar(self):
        main_map = self.main_class()
        ndim = np.ceil(self.dimg/self.win_size).astype(int)
        # mask Maps_sar
        climate_Maps_sar = self.geocode_sar()
        climate_Maps_sar = np.where(main_map!=0, climate_Maps_sar, np.NaN)
        climate_Maps_sar = np.where(climate_Maps_sar>0, climate_Maps_sar, np.NaN)
        climate_id = np.zeros(ndim)
        for ii in np.arange(int(ndim[0])):
            for jj in np.arange(int(ndim[1])):
                ind_ii = int(ii*self.win_size)
                ind_jj = int(jj*self.win_size)
                sub_climate_Maps_sar = climate_Maps_sar[ind_ii:int(ind_ii+self.win_size), ind_jj:int(ind_jj+self.win_size)]
                climate_id[ii, jj] = stats.mode(sub_climate_Maps_sar,axis=None)[0][0]
        climate_id = self.cor_shape(climate_id)
        return climate_id
    
    def cor_shape(self, dataset):
        dim = np.floor(self.dimg/self.win_size)
        if dataset.shape[0]>dim[0]:
            dataset = dataset[:-1,:]
        if dataset.shape[1]>dim[1]:
            dataset = dataset[:,:-1]
        return dataset
    
    def plt_dicts(self):
        col_dict = dict(zip(np.linspace(1,30,30), [(0/255, 0/255, 255/255), (0/255,120/255,255/255), (70/255, 170/255, 250/255), (255/255, 0/255, 0/255), 
                                           (255/255, 150/255, 150/255), (245/255, 165/255, 0/255), (255/255, 220/255, 100/255), (255/255, 255/255, 0/255),
                                           (200/255, 200/255, 0/255), (150/255, 150/255, 0/255), (150/255, 255/255, 150/255), (100/255, 200/255, 100/255),
                                           (50/255, 150/255, 50/255), (200/255, 255/255, 80/255), (100/255, 255/255, 50/255), (50/255, 200/255, 0/255),
                                           (255/255, 0/255, 255/255), (200/255, 0/255, 200/255), (150/255, 50/255, 150/255), (150/255, 100/255, 150/255),
                                           (170/255, 175/255, 255/255), (90/255, 120/255, 220/255), (75/255, 80/255, 180/255), (50/255, 0/255, 135/255),
                                           (0/255, 255/255, 255/255), (55/255, 200/255, 255/255), (0/255, 125/255, 125/255),
                                           (0/255, 70/255, 95/255), (0.3, 0.3, 0.3), (0.6, 0.6, 0.6)]))
        label_dict = dict(zip(np.linspace(1,30,30), ['Af','Am','Aw','BWh','BWk','BSh','BSk','Csa','Csb','Csc','Cwa','Cwb','Cwc','Cfa','Cfb','Cfc',
                   'Dsa','Dsb','Dsc','Dsd','Dwa','Dwb','Dwc','Dwd','Dfa','Dfb','Dfc','Dfd', 'ET', 'EF']))
        return col_dict, label_dict