import numpy as np
from scipy import constants as cnst

class tpt_SM(object):
    """
    A preliminary soil moisture modeled closure phase at C-band
    """

    def __init__(self, SM, S, C, inc_deg, version):
        """Relative dielectric constant: equation (14) in [Hallikainen, 1984]
        sm: volumetric soil moisture [0, 1]
        S: sand textural components of a soil
        C: clay textural components of a soil
        https://www.researchgate.net/figure/Soil-characteristics-of-Stein-Oukoop-and-Zegveld_tbl3_234076274"""
        self.sm = SM
        self.S = S
        self.C = C
        self.inc = inc_deg
        self.version = version
            
    def sim_model(self):
        model = np.zeros(self.SM.shape[0]-2)
        for ii in np.arange(self.SM.shape[0]-2):
            model[ii] = (-(self.SM[ii]-self.SM[ii+1])*(self.SM[ii+1]-self.SM[ii+2])*(self.SM[ii+2]-self.SM[ii]))
        return model
    
    def dielec_4(self):
        a0r = 1.993
        a1r = 0.002
        a2r = 0.015
        b0r = 38.086
        b1r = -0.176
        b2r = -0.633
        c0r = 10.720
        c1r = 1.256
        c2r = 1.522
        epsilon_re = (a0r + a1r * self.S + a2r * self.C) + (b0r + b1r * self.S + b2r * self.C) * self.SM + (c0r + c1r * self.S + c2r * self.C) * self.SM ** 2
        a0i = -0.123
        a1i = 0.002
        a2i = 0.003
        b0i = 7.502
        b1i = -0.058
        b2i = -0.116
        c0i = 2.942
        c1i = 0.452
        c2i = 0.543
        epsilon_img = (a0i + a1i * self.S + a2i * self.C) + (b0i + b1i * self.S + b2i * self.C) * self.SM + (c0i + c1i * self.S + c2i * self.C) * self.SM ** 2
        #     mistake in equation 8
        return epsilon_re - 1j * epsilon_img
    
    def dielec_6(self):
        a0r = 2.927
        a1r = -0.012
        a2r = -0.001
        b0r = 5.505
        b1r = 0.371
        b2r = 0.062
        c0r = 114.826
        c1r = -0.389
        c2r = -0.547
        epsilon_re = (a0r + a1r * self.S + a2r * self.C) + (b0r + b1r * self.S + b2r * self.C) * self.SM + (c0r + c1r * self.S + c2r * self.C) * self.SM ** 2
        a0i = 0.004
        a1i = 0.001
        a2i = 0.002
        b0i = 0.951
        b1i = 0.005
        b2i = -0.010
        c0i = 16.759
        c1i = 0.192
        c2i = 0.290
        epsilon_img = (a0i + a1i * self.S + a2i * self.C) + (b0i + b1i * self.S + b2i * self.C) * self.SM + (c0i + c1i * self.S + c2i * self.C) * self.SM ** 2
        return epsilon_re - 1j * epsilon_img
    
    def dielec_C(self):
        # 5 GHz
        epsilon = (self.dielec_4()+self.dielec_6())*cnst.epsilon_0/2
        # 5.5 GHz
        epsilon = (epsilon/cnst.epsilon_0+self.dielec_6())/2*cnst.epsilon_0
        # 5.4 GHz
        epsilon = epsilon - (epsilon - self.dielec_4()*cnst.epsilon_0)/5
        return epsilon
    
    def kz(self):
        f0 = 5.405e9
        # refraction direction
        epsilon_r = self.dielec_C()
        sin_theta_i = np.sin(np.radians(self.inc_deg)) # incident angle
        kx_air = 2 * np.pi * sin_theta_i * f0 /cnst.speed_of_light # kx in the air
        omega = 2 *np.pi * f0
        kz_soil = np.sqrt(omega **2 * epsilon_r * cnst.mu_0 - kx_air ** 2)# kz in the soil eq.5 in Francesco et al, 2012
        return kz_soil
    
    def ifg_model(self):
        model = []
        kz = self.kz()
        for ii in np.arange(kz.shape[0]-2):
            I12 = 1/(2j*(kz[ii]-np.conj(kz[ii+1])))
            I23 = 1/(2j*(kz[ii+1]-np.conj(kz[ii+2])))
            I31 = 1/(2j*(kz[ii+2]-np.conj(kz[ii])))
            model.append(np.angle(I12*I23*I31))
        return np.asarray(model)
    
    def tpt(self):
        if self.version == 'simple':
            return self.sim_model()
        elif self.version == 'full':
            return self.ifg_model()