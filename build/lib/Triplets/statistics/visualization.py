import os
import glob
import time
import cartopy
import numpy as np
import xarray as xr
import pandas as pd
from osgeo import gdal
from cartopy import config
import cartopy.crs as ccrs
import scipy.stats as stats
from matplotlib import colors
from matplotlib import ticker
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature
from Triplets.application.Land_cover import Landcover_match
from Triplets.application.Climate_zone import Climate_match
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

def load_data(tpt_folder, stack_name, win_size, data_type, filename):
    path = os.path.join(tpt_folder, stack_name, str(win_size * 20), data_type, 'data', filename)
    return xr.open_dataarray(path).data

def load_segments(tpt_folder, stack_name, win_size, pol):
    if pol == 'VV':
        lat = load_data(tpt_folder, stack_name, win_size, 'geocode', 'lat.nc')
        lon = load_data(tpt_folder, stack_name, win_size, 'geocode', 'lon.nc')
        P = load_data(tpt_folder, stack_name, win_size, 'statistics', 'Pearsonr_baseline.nc')
        Pp = load_data(tpt_folder, stack_name, win_size, 'statistics', 'PearsonrP_baseline.nc')
        mean_tpt = load_data(tpt_folder, stack_name, win_size, 'statistics', 'Temporal_mean.nc')
        mask = load_data(tpt_folder, stack_name, win_size, 'number_of_looks', 'ENL.nc')
        land = load_data(tpt_folder, stack_name, win_size, 'statistics', 'land_map.nc')
        climate = load_data(tpt_folder, stack_name, win_size, 'statistics', 'climate_map.nc')
        return lat, lon, P, Pp, mean_tpt, mask, land, climate
    elif pol == 'VH':
        P = load_data(tpt_folder, stack_name, win_size, 'statistics', 'Pearsonr_baseline.nc')
        Pp = load_data(tpt_folder, stack_name, win_size, 'statistics', 'PearsonrP_baseline.nc')
        mean_tpt = load_data(tpt_folder, stack_name, win_size, 'statistics', 'Temporal_mean.nc')
        return P, Pp, mean_tpt

def plot_single(lat_b, lon_b, data_b, mask_b, lat_m, lon_m, data_m, mask_m, lat_u, lon_u, data_u, mask_u):
    plt.style.use('ieee')
    fig = plt.figure(figsize=(12, 18))
    img_extent = [np.min(np.array([lon_b.min(), lon_m.min(), lon_u.min()])), 
              np.max(np.array([lon_b.max(), lon_m.max(), lon_u.max()])), 
              lat_b.min(), lat_u.max()]
    ax = plt.axes(projection=ccrs.Mercator())
    ax.set_extent(img_extent, ccrs.PlateCarree())
    draw_labels = True
    gl = ax.gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    xlabels_top=False
    xlabels_bottom=True
    ylabels_left=False
    ylabels_right=True
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    sea_10m = cartopy.feature.NaturalEarthFeature('physical', 'ocean', '10m',
                                                    edgecolor='face',
                                                    facecolor=cartopy.feature.COLORS['water'])

    ax.add_feature(sea_10m, zorder=1)
    im = ax.pcolormesh(lon_b, lat_b, np.ma.masked_array(data_b, np.isnan(mask_b)), transform=ccrs.PlateCarree(), zorder=1, cmap= cm, norm=norm)
    im = ax.pcolormesh(lon_m, lat_m, np.ma.masked_array(data_m, np.isnan(mask_m)), transform=ccrs.PlateCarree(), zorder=1, cmap= cm, norm=norm)
    im = ax.pcolormesh(lon_u, lat_u, np.ma.masked_array(data_u, np.isnan(mask_u)), transform=ccrs.PlateCarree(), zorder=1, cmap= cm, norm=norm)
    cbar_ax = fig.add_axes([0.95, 0.13, 0.02, 0.75])
    diff = norm_bins[1:] - norm_bins[:-1]
    tickz = norm_bins[:-1] + diff / 2
    clb = fig.colorbar(im, cax=cbar_ax, format=fmt, ticks=tickz)
    ax.set_ylabel('Latitude [deg]', fontsize=20)
    ax.set_xlabel('Longitude [deg]', fontsize=20)
    
def plot_double(lat_b, lon_b, data_vvb, data_vhb, lat_m, lon_m, data_vvm, data_vhm, lat_u, lon_u, data_vvu, data_vhu):
    plt.style.use('ieee')
    texts = ['(a)', '(b)']
    xlabels_top=False
    xlabels_bottom=True
    ylabels_left=False
    ylabels_right=True
    fig, ax = plt.subplots(nrows=1, ncols=2, subplot_kw={'projection': ccrs.Mercator()}, figsize=(12, 18))
    img_extent = [np.min(np.array([lon_b.min(), lon_m.min(), lon_u.min()])), 
              np.max(np.array([lon_b.max(), lon_m.max(), lon_u.max()])), 
              lat_b.min(), lat_u.max()]
    sea_10m = cartopy.feature.NaturalEarthFeature('physical', 'ocean', '10m',
                                                    edgecolor='face',
                                                    facecolor=cartopy.feature.COLORS['water'])
    ax[0].set_extent(img_extent, ccrs.PlateCarree())
    draw_labels = True
    gl = ax[0].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[0].add_feature(sea_10m, zorder=1)
    # ax[0].set_title('Boxcar Multilooking', y=1.03, fontsize=12)
    ax[0].annotate(texts[0], xy=(0.5, -0.08), xycoords="axes fraction", fontsize=12)
    im1 = ax[0].pcolormesh(lon_b, lat_b, np.degrees(np.where(data_vvb==0, np.NaN, data_vvb)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')
    im1 = ax[0].pcolormesh(lon_m, lat_m, np.degrees(np.where(data_vvm==0, np.NaN, data_vvm)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')
    im1 = ax[0].pcolormesh(lon_u, lat_u, np.degrees(np.where(data_vvu==0, np.NaN, data_vvu)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')

    # ax[1] = plt.axes(projection=ccrs.Mercator())
    ax[1].set_extent(img_extent, ccrs.PlateCarree())
    gl = ax[1].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[1].add_feature(sea_10m, zorder=1)
    im2 = ax[1].pcolormesh(lon_b, lat_b, np.degrees(np.where(data_vhb==0, np.NaN, data_vhb)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')
    im2 = ax[1].pcolormesh(lon_m, lat_m, np.degrees(np.where(data_vhm==0, np.NaN, data_vhm)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')
    im2 = ax[1].pcolormesh(lon_u, lat_u, np.degrees(np.where(data_vhu==0, np.NaN, data_vhu)), transform=ccrs.PlateCarree(), zorder=1, vmin=-15, vmax=15, cmap='bwr')

    # ax[1].set_title('Adaptive Multilooking', y=1.03, fontsize=12)
    ax[1].annotate(texts[1], xy=(0.5, -0.08), xycoords="axes fraction", fontsize=12)
    # cbar_ax = fig.add_axes([0.07, 0.185, 0.02, 0.635])
    # clb = fig.colorbar(im1, cax=cbar_ax)
    cbar_ax = fig.add_axes([0.95, 0.25, 0.03, 0.5])
    clb = fig.colorbar(im2, cax=cbar_ax)
    clb.set_label('$\phi_{nkm}$ [deg]', fontsize=12, labelpad=-34, y=1.03, rotation=0)
    ax[0].set_title('VV', fontsize = 12)
    ax[1].set_title('VH', fontsize = 12)
    
def plot_quad(lat_b, lon_b, data_vvb1, data_vvb2, data_vhb1, data_vhb2, lat_m, lon_m, data_vvm1, data_vvm2, data_vhm1, data_vhm2, lat_u, lon_u, data_vvu1, data_vvu2, data_vhu1, data_vhu2):
    plt.style.use('ieee')
    xlabels_top=False
    xlabels_bottom=True
    ylabels_left=False
    ylabels_right=True
    texts = ['(a)', '(b)', '(c)', '(d)']
    fig, ax = plt.subplots(nrows=1, ncols=4, subplot_kw={'projection': ccrs.Mercator()}, figsize=(16, 9))
    img_extent = [np.min(np.array([lon_b.min(), lon_m.min(), lon_u.min()])), 
                  np.max(np.array([lon_b.max(), lon_m.max(), lon_u.max()])), 
                  lat_b.min(), lat_u.max()]
    sea_10m = cartopy.feature.NaturalEarthFeature('physical', 'ocean', '10m',
                                                    edgecolor='face',
                                                    facecolor=cartopy.feature.COLORS['water'])
    ax[0].set_extent(img_extent, ccrs.PlateCarree())
    draw_labels = True
    gl = ax[0].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[0].add_feature(sea_10m, zorder=1)
    # ax[0].set_title('Boxcar Multilooking', y=1.03, fontsize=12)
    ax[0].annotate(texts[0], xy=(0.45, -0.08), xycoords="axes fraction", fontsize=12)
    im1 = ax[0].pcolormesh(lon_b, lat_b, data_vvb1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')
    im1 = ax[0].pcolormesh(lon_m, lat_m, data_vvm1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')
    im1 = ax[0].pcolormesh(lon_u, lat_u, data_vvu1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')


    # ax[1] = plt.axes(projection=ccrs.Mercator())
    ax[1].set_extent(img_extent, ccrs.PlateCarree())
    gl = ax[1].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[1].add_feature(sea_10m, zorder=1)
    im2 = ax[1].pcolormesh(lon_b, lat_b, data_vhb1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')
    im2 = ax[1].pcolormesh(lon_m, lat_m, data_vhm1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')
    im2 = ax[1].pcolormesh(lon_u, lat_u, data_vhu1, transform=ccrs.PlateCarree(), zorder=1, vmin=-1, vmax=1, cmap='bwr')
    cbar_ax = fig.add_axes([0.05, 0.175, 0.02, 0.65])
    clb = fig.colorbar(im2, ax=ax[1], cax=cbar_ax)
    clb.set_label('Pearsonr', fontsize=12, labelpad=-42, y=1.05, rotation=0)

    # ax[1].set_title('Adaptive Multilooking', y=1.03, fontsize=12)
    ax[1].annotate(texts[1], xy=(0.45, -0.08), xycoords="axes fraction", fontsize=12)
    # cbar_ax = fig.add_axes([0.07, 0.185, 0.02, 0.635])
    # clb = fig.colorbar(im1, cax=cbar_ax)
    # cbar_ax = fig.add_axes([0.94, 0.535, 0.02, 0.35])
    # clb = fig.colorbar(im2, cax=cbar_ax)
    # clb=fig.colorbar(im2)
    clb.set_label('Pearsonr', fontsize=12, labelpad=-42, y=1.05, rotation=0)
    ax[0].set_title('VV', fontsize = 12)
    ax[1].set_title('VH', fontsize = 12)

    ax[2].set_extent(img_extent, ccrs.PlateCarree())
    gl = ax[2].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[2].add_feature(sea_10m, zorder=1)
    im3 = ax[2].pcolormesh(lon_b, lat_b, data_vvb2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    im3 = ax[2].pcolormesh(lon_m, lat_m, data_vvm2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    im3 = ax[2].pcolormesh(lon_u, lat_u, data_vvu2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    ax[2].annotate(texts[2], xy=(0.45, -0.08), xycoords="axes fraction", fontsize=12)

    ax[3].set_extent(img_extent, ccrs.PlateCarree())
    gl = ax[3].gridlines(crs=ccrs.PlateCarree(), color='gray', draw_labels=draw_labels)
    if draw_labels:
        gl.top_labels = xlabels_top
        gl.left_labels = ylabels_left
        gl.bottom_labels = xlabels_bottom
        gl.right_labels = ylabels_right
        gl.xlines = True
        gl.ylines = True
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 12}
        gl.ylabel_style = {'size': 12}
    ax[3].add_feature(sea_10m, zorder=1)
    im4 = ax[3].pcolormesh(lon_b, lat_b, data_vhb2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    im4 = ax[3].pcolormesh(lon_m, lat_m, data_vhm2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    im4 = ax[3].pcolormesh(lon_u, lat_u, data_vhu2, transform=ccrs.PlateCarree(), zorder=1, vmin=0, vmax=1, cmap='inferno')
    ax[3].annotate(texts[3], xy=(0.45, -0.08), xycoords="axes fraction", fontsize=12)
    cbar_ax = fig.add_axes([0.95, 0.175, 0.02, 0.65])
    clb = fig.colorbar(im4, ax=ax[3], cax=cbar_ax)
    clb.set_label('P-value', fontsize=12, labelpad=-30, y=1.05, rotation=0)
    ax[2].set_title('VV', fontsize = 12)
    ax[3].set_title('VH', fontsize = 12)