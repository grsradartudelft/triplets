import os
import glob
import gdal
import imageio
import numpy as np
import xarray as xr
import seaborn as sb
import drama.utils as drtls
import matplotlib.pyplot as plt


class tpt_statistic(object):

    def __init__(self, tpt_folder, tpt_stack_name, tiff_folder, stack_name, pol, rml):
        self.tpt_folder = tpt_folder
        self.tpt_stack_name = tpt_stack_name
        self.tiff_folder = tiff_folder
        self.stack_name = stack_name

        path = os.path.join(self.tiff_folder, self.stack_name, 'interferogram')
        self.proj = os.listdir(path)[0]
        self.res = int(self.proj.split('0_')[1])

        self.pol = pol
        self.rml = rml

        self.tpt_dates = self.list_of_tpt_dates()

        if self.rml != 2:
            res = int(self.res * self.rml / 2)
            self.lat = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lat.nc').data
            self.lon = xr.open_dataarray(
                self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/geocode/' + str(res) + '_lon.nc').data
        else:
            path = os.path.join(self.tiff_folder, self.stack_name, 'geocode', self.proj)
            paths = os.listdir(path)
            self.lat = gdal.Open(os.path.join(path, paths[0])).ReadAsArray()
            self.lon = gdal.Open(os.path.join(path, paths[1])).ReadAsArray()

    def load_data(self, dates, data_type=''):
        '''
        load data
        :param dates:
        :param data_type:
        :return:
        '''
        if data_type == 'triplets' or data_type == 'number_of_looks':
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/' + data_type + '/' + 'data/'
            os.chdir(path)
            filename = glob.glob(dates + '_' + data_type[0] + '*')
            data = xr.open_dataarray(os.path.join(path, filename[0])).data
            return data
        elif data_type == 'geocode':
            path = self.tiff_folder + '/' + self.stack_name + '/' + data_type + '/' + self.proj + '/'
            paths = os.listdir(path)
            lat = gdal.Open(path + paths[0]).ReadAsArray()
            lon = gdal.Open(path + paths[1]).ReadAsArray()
            return lat, lon
        else:
            path = self.tiff_folder + '/' + self.stack_name + '/' + data_type + '/' + self.proj + '/'
            os.chdir(path)
            filename = glob.glob(dates[0] + '_' + data_type[0] + '*')
            data = gdal.Open(path + filename[0]).ReadAsArray()
            return data

    def list_of_tpt_dates(self):
        '''
        list of triplets dates
        :return:
        '''
        tptdates = os.listdir(self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data')
        dates = []
        for i in tptdates:
            dates.append(i.split('_p')[0])
        return dates

    def tpt_temporal_statistics(self):
        '''
        compute different statistics for triplets phase
        :param statistic_type:
        :return:
        '''
        num = len(self.tpt_dates)
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        if self.rml != 2:
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/'
            rsmp = int(self.rml / 2)
            nlooks_mask = drtls.smooth(drtls.smooth(nlooks_mask, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:,
                          ::rsmp]
        else:
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/'
        filenames = os.listdir(path)
        mean_tpt = 0
        max_tpt = 0
        min_tpt = 100
        for filename in filenames:
            tpt_data = xr.open_dataarray(os.path.join(path, filename)).data
            mean_tpt = mean_tpt + tpt_data / num
            max_tpt = np.maximum(max_tpt, tpt_data)
            min_tpt = np.minimum(min_tpt, tpt_data)
        return min_tpt, max_tpt, mean_tpt, nlooks_mask

    def tpt_temporal_std(self):
        #         make mask
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        if self.rml != 2:
            res = int(self.res * self.rml / 2)  # resolution
            data_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(res) + '_mean.nc'
            image_path_std = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + str(
                res) + '_std.png'
            data_path_std = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(
                res) + '_std.nc'
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/'
            rsmp = int(self.rml / 2)
            nlooks_mask = drtls.smooth(drtls.smooth(nlooks_mask, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:,
                          ::rsmp]
        else:
            res = self.res
            data_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(
                res) + '_mean.nc'
            image_path_std = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + str(
                res) + '_std.png'
            data_path_std = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(
                res) + '_std.nc'
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/'
        #         compute standard deviation
        mean_tpt = xr.open_dataarray(data_path_mean).data
        num = len(self.tpt_dates)
        filenames = os.listdir(path)
        std_tpt = 0
        for filename in filenames:
            tpt_data = xr.open_dataarray(os.path.join(path, filename)).data
            std_tpt = std_tpt + (tpt_data - mean_tpt) ** 2
        std_tpt = np.sqrt(std_tpt / num)
        #         plot std
        plt.figure(figsize=(12, 9), dpi=300)
        plt.pcolormesh(self.lon, self.lat, np.ma.masked_array(np.degrees(std_tpt), np.logical_not(nlooks_mask != 0)),
                       cmap='plasma', vmin=0)
        clb = plt.colorbar()
        clb.set_label('$\sigma_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
        plt.grid()
        plt.ylabel('Latitude [deg]', fontsize=20)
        plt.xlabel('Longitude [deg]', fontsize=20)
        plt.title('Closure phase standard deviation', fontsize=20, y=1.02)
        plt.savefig(image_path_std, dpi=300)
        plt.close()
        #         save std
        std_tpt = xr.DataArray(std_tpt)
        std_tpt.to_netcdf(data_path_std)

    def save_plot_statistic(self):

        min_tpt, max_tpt, mean_tpt, nlooks_mask = self.tpt_temporal_statistics()

        if self.rml != 2:
            res = int(self.res * self.rml / 2)  # resolution
            image_path_min = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + str(res) + '_min.png'
            data_path_min = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(res) + '_min.nc'
            image_path_max = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + str(res) + '_max.png'
            data_path_max = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(res) + '_max.nc'
            image_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + str(res) + '_mean.png'
            data_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(res) + '_mean.nc'
        else:
            res = self.res
            image_path_min = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + str(
                res) + '_min.png'
            data_path_min = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(
                res) + '_min.nc'
            image_path_max = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + str(
                res) + '_max.png'
            data_path_max = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(
                res) + '_max.nc'
            image_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + str(
                res) + '_mean.png'
            data_path_mean = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(
                res) + '_mean.nc'

        # plot statistics
        plt.figure(figsize=(12, 9), dpi=300)
        plt.pcolormesh(self.lon, self.lat, np.ma.masked_array(np.degrees(min_tpt), np.logical_not(nlooks_mask != 0)),
                       cmap='plasma', vmin=-180)
        clb = plt.colorbar()
        clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
        plt.grid()
        plt.ylabel('Latitude [deg]', fontsize=20)
        plt.xlabel('Longitude [deg]', fontsize=20)
        plt.title('Minimum closure phase', fontsize=20, y=1.02)
        plt.savefig(image_path_min, dpi=300)
        plt.close()

        plt.figure(figsize=(12, 9), dpi=300)
        plt.pcolormesh(self.lon, self.lat, np.ma.masked_array(np.degrees(max_tpt), np.logical_not(nlooks_mask != 0)),
                       cmap='plasma', vmax=180)
        clb = plt.colorbar()
        clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
        plt.grid()
        plt.ylabel('Latitude [deg]', fontsize=20)
        plt.xlabel('Longitude [deg]', fontsize=20)
        plt.title('Maximum closure phase', fontsize=20, y=1.02)
        plt.savefig(image_path_max, dpi=300)
        plt.close()

        plt.figure(figsize=(12, 9), dpi=300)
        plt.pcolormesh(self.lon, self.lat, np.ma.masked_array(np.degrees(mean_tpt), np.logical_not(nlooks_mask != 0)),
                       cmap='plasma', vmin=-10, vmax=10)
        clb = plt.colorbar()
        clb.set_label('$\Phi_{123}$ [deg]', fontsize=15, labelpad=-40, y=1.05, rotation=0)
        plt.grid()
        plt.ylabel('Latitude [deg]', fontsize=20)
        plt.xlabel('Longitude [deg]', fontsize=20)
        plt.title('Mean closure phase', fontsize=20, y=1.02)
        plt.savefig(image_path_mean, dpi=300)
        plt.close()

        # save statistics
        mean_tpt = xr.DataArray(mean_tpt)
        mean_tpt.to_netcdf(data_path_mean)

        min_tpt = xr.DataArray(min_tpt)
        min_tpt.to_netcdf(data_path_min)

        max_tpt = xr.DataArray(max_tpt)
        max_tpt.to_netcdf(data_path_max)

    def density_plot_animation(self):
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        if self.rml != 2:
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/'
            rsmp = int(self.rml / 2)
            nlooks_mask = drtls.smooth(drtls.smooth(nlooks_mask, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:,
                          ::rsmp]
            gif_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/ani/' + str(int(self.res * self.rml / 2)) + '_hist.gif'
        else:
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/'
            gif_path = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/ani/' + str(self.res) + '_hist.gif'
        filenames = os.listdir(path)
        image_list = []
        for filename in filenames:
            tpt_phase = xr.open_dataarray(os.path.join(path, filename)).data
            dates = filename[0:26]
            if self.rml != 2:
                res = int(self.res * self.rml / 2)  # resolution
                im_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + '_' + dates + '_' + str(res) + '_his.png'
            else:
                res = self.res
                im_path = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + '_' + dates + '_' + str(res) + '_his.png'
            plt.figure(figsize=(12, 9), dpi=300)
            sb.distplot(np.degrees(tpt_phase[nlooks_mask != 0]), bins=72, hist=False, color='k')
            plt.grid()
            plt.xlabel('$\Phi_{123}$[deg]', fontsize=15)
            plt.xlim([-180, 180])
            plt.ylim([0, 0.05])
            plt.title(dates, fontsize=20, y=1.02)
            plt.savefig(im_path, dpi=300)
            plt.close()
            image_list.append(imageio.imread(im_path))
        imageio.mimwrite(gif_path,image_list, format='GIF', duration=0.5)

    def convert_date_to_month(self, month):
        if month == '01':
            return 'January'
        elif month == '02':
            return 'February'
        elif month == '03':
            return 'March'
        elif month == '04':
            return 'April'
        elif month == '05':
            return 'May'
        elif month == '06':
            return 'June'
        elif month == '07':
            return 'July'
        elif month == '08':
            return 'August'
        elif month == '09':
            return 'September'
        elif month == '10':
            return 'October'
        elif month == '11':
            return 'November'
        else:
            return 'December'

    def unique_month(self, month_list):

        num = [0]
        unique_month = [month_list[0]]

        for ind in np.arange(len(month_list) - 1):
            if month_list[ind + 1] != month_list[ind]:
                unique_month.append(month_list[ind + 1])
                num.append(ind + 1)
        return unique_month, num

    def threeD_density_plot(self):
        '''
        generate 3d density plot: with dimensions: bin_edge, time, histogram.
        :return:
        '''

        # define the first dataset as the mask for all stack
        nlooks_path = self.tpt_folder + '/' + self.tpt_stack_name + '/number_of_looks/data/'
        nlooks_mask = xr.open_dataarray(nlooks_path + os.listdir(nlooks_path)[0]).data
        if self.rml != 2:
            res = int(self.res * self.rml / 2)  # resolution
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/'
            rsmp = int(self.rml / 2)
            nlooks_mask = drtls.smooth(drtls.smooth(nlooks_mask, self.rml, axis=0)[::rsmp, :], self.rml, axis=1)[:,
                          ::rsmp]
            im_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/img/' + str(
                res) + '_density.png'
            data_path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/statistics/data/' + str(
                res) + '_density.nc'
        else:
            path = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/'
            im_path = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/img/' + str(self.res) + '_density.png'
            data_path = self.tpt_folder + '/' + self.tpt_stack_name + '/statistics/data/' + str(self.res) + '_density.nc'

        filenames = os.listdir(path)

        # define bins
        bin_edge = np.linspace(-180, 180, 361)
        num = len(filenames)
        density = np.zeros([num, len(bin_edge) - 1])
        yticks_label = []
        for ii in np.arange(num):
            if self.rml != 2:
                path = self.tpt_folder + '/' + self.tpt_stack_name + '/multi_looking/triplets/data/' + filenames[ii]
            else:
                path = self.tpt_folder + '/' + self.tpt_stack_name + '/triplets/data/' + filenames[ii]
            tpt_phase = xr.open_dataarray(path).data
            density[ii, :] = np.histogram(np.degrees(tpt_phase[nlooks_mask != 0]), bins=bin_edge, density=True)[0]
            yticks_label.append(self.convert_date_to_month(filenames[ii][13:15]))
        density = xr.DataArray(density)
        density.to_netcdf(data_path)

        # plot the 3d density plot
        unique_month, number = self.unique_month(yticks_label)
        fig = plt.figure(figsize=(20, 9), dpi=300)
        im = plt.imshow(density, origin='auto', cmap='plasma', extent=[-180, 180, 0, num], vmin=0, vmax=0.05)
        plt.xlabel('$\Phi_{123}$[deg]', fontsize=15)
        plt.xlim([-25, 25])
        plt.yticks(number, unique_month, fontsize=15)
        plt.title(filenames[0][9:17] + '_' + filenames[-1][9:17], fontsize=20, y=1.02)
        clb = plt.colorbar()
        clb.set_label('Density', fontsize=15, labelpad=-42, y=1.04, rotation=0)
        plt.savefig(im_path, dpi=300)
        plt.close()


